(function (global, App) {
    'use strict';

    /* global EQ, moment, debug */

    var Backbone = global.Backbone,
        _ = global._;

    /**
    * Models
    */

    var WeekDayModel = Backbone.Model.extend({
        defaults: {
            specialClass: '',
            displayTime: ''
        }
    });

    /**
    * Views Helpers
    */

    var DaySingleViewHelpers = {
        isCurrentDay: function () {
            return moment(this.date.format('YYYY-MM-DD')).isSame(moment().format('YYYY-MM-DD')) ? true : false;
        },
        getDateString: function (dateString) {
            return EQ.Helpers.dateTime.convertDateToString(dateString);
        },
        getInstructors: function (instructors) {
            var instructorsString;

            if (instructors.length) {
                _.each(instructors, function (instructor) {
                    if (_.isNull(instructor.substitute)) {
                        instructorsString = instructor.instructor.firstName + ' ' + instructor.instructor.lastName;
                    } else {
                        instructorsString = instructor.substitute.firstName + ' ' + instructor.substitute.lastName + ' (SUB)';
                    }
                });
            }

            return instructorsString;
        },
        getEventDuration: function (startDate, endDate) {
            return EQ.Helpers.dateTime.getTimeRange(startDate, endDate);
        },
        isPastday: function (startLocal) {
            return moment().isAfter(moment(startLocal)) ? 'pastEvent' : '';
        }
    };

    /**
    * Views
    */

    var CalendarView = Backbone.View.extend({
        tagName: 'table',
        initialize: function (options) {
            this.weekData = options.weekData;
            this.hasTrainer = options.hasTrainer;
        },
        render: function () {
            debug('SMALL CALENDAR', this.weekData);
            _.each(this.weekData, function (dayDetail) {
                var daySingleView = new DaySingleView({ model: new WeekDayModel(dayDetail) });
                this.$el.append(daySingleView.render().el);
            }, this);
            return this;
        }
    });

    var DaySingleView = Backbone.View.extend({
        tagName: 'tr',
        className: 'day-detail-container',
        template: _.template($('#dayTemplate').html()),
        events: {
            'click .add-class': 'openOverlay',
            'click .close-event-overlay': 'closeOverlay'
        },
        openOverlay: function () {
            this.$el.addClass('active');
        },
        closeOverlay: function () {
            this.$el.removeClass('active');
        },
        getRenderData: function () {
            var data;
            this.model.set('hasTrainer', this.hasTrainer);

            data = this.model.toJSON();
            console.log(data);
            return _.extend(data, DaySingleViewHelpers);
        },
        render: function () {
            this.$el.html(this.template(this.getRenderData()));
            return this;
        }
    });

    var WeekCalendar = {};

    WeekCalendar.publicMethods = {
        update: function (options) {
            WeekCalendar.calendarView.remove();
            WeekCalendar.calendarView = new CalendarView({ weekData: options.weekData, hasTrainer: options.hasTrainer});
            WeekCalendar.$el.append(WeekCalendar.calendarView.render().el);
        }
    };

    WeekCalendar.init = function ($el, options) {
        this.$el = $el;
        this.calendarView = new CalendarView({ weekData: options.weekData, hasTrainer: options.hasTrainer});
        $el.append(this.calendarView.render().el);

         // Bind public methods
        $el.data('publicMethods', this.publicMethods);
    };

    /**
    * Component Init.
    */

    App.Components['week-calendar'] = function ($el, options) {
        WeekCalendar.init($el, options);
    };

} (window, window.App));