(function (App) {
    'use strict';

    /**
     * Inline select module handler.
     */
    App.Components['inline-select'] = function ($el, type) {

        if (type === 'clubslist') {
            /* global allRegionsData, EQ */
            // Get all clubs
            var options = allRegionsData.reduce(function (prev, nextRegion) {
                return prev.concat(EQ.Helpers.getAllFacilities(nextRegion));
            // Sort alphabetically ascendent
            }, []).sort(function (a, b) {
                return a.ClubName < b.ClubName ? -1 : 1;
            // Map to <option>
            }).map(function (club) {
                return '<option data-facility-id="' + club.Id + '" value="' + club.ClubID + '">' + club.ClubName + '</option>';
            });

            $el.find('select').append(options);
            var $selectedValue = $('#selectedValue', $el);
            if ($selectedValue.length > 0 && $selectedValue.val() !== '') {
                $el.find('select option[value="' + $selectedValue.val() + '"]').attr('selected', 'selected');
            }
        }

        var $optionPlaceholder = $el.find('span.option');

        // Set the initial option.
        $optionPlaceholder.text($el.find('option:selected').text());

        $el.find('select').on('change', function (e) {
            console.log(e, $el.find('option:selected').text());
            $optionPlaceholder.removeClass('open');
            // lets deprecate this damn thing shall we? add data-skip=true
            // to use the new jquery plugin selectWrapper instead.
            if (!$(this).attr('data-skip')) {
                $optionPlaceholder.text($el.find('option:selected').text());
                // Pass the event to the wrapper.
                $el.trigger('change', e);
                // Remove focus from select
                $el.find('select').blur();
            }
        });

        $el.find('select').on('focus', function () {
            console.log('select');
            $optionPlaceholder.addClass('open');
        });

        $el.find('select').on('blur', function () {
            console.log('blur');
            $optionPlaceholder.removeClass('open');
        });
    };

}(window.App));