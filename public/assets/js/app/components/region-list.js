(function (global, App) {
    'use strict';

    /* global EQ */

    App.Components['region-list'] = function ($el) {
        var $regions = $('<ul></ul>'),
            RegionsData = global.allRegionsData;

        //Regions
        var USRegions = [],
            INTRegions = [];

        //Loop US Regions first.
        $.each(RegionsData, function (j, region) {
            var $regionTemplate = $('<li><img src="" /><a href=""></a></li>'),
                facilities = EQ.Helpers.getAllFacilities(region),
                link;

            if (facilities.length === 1) {
                link = App.Pages.Clubs.Club.getLink(facilities[0]);
            } else {
                link = '/regions/' + region.ShortName;
            }

            $regionTemplate.find('a').attr('href', link).text(region.CircleName);

            $regionTemplate.find('img').attr('src', region.BackgroundImageSmall);


            if (region.Country === 'US') {
                USRegions.push($regionTemplate);
            } else {
                INTRegions.push($regionTemplate);
            }
        });

        //All Regions
        var ALLRegions = USRegions.concat(INTRegions);

        $el.append($regions.append(ALLRegions));
    };


}(window, window.App));