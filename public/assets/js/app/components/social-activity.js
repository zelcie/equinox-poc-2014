(function (global, App) {
    'use strict';

    /* global APIEndpoint, debug, EQ, moment */

    var Backbone = global.Backbone,
        _ = global._;

    /**
    * Models
    */

    var FBfriend = Backbone.Model.extend({
        defaults: {
            firstName: '',
            lastName: '',
            homeFacilityName: '',
            facility: {
                id: null,
                clubId: null,
                name: '',
                shortName: '',
                mobileName: null,
                webName: null,
                timeZoneId: null
            }
        }
    });

    var FBfriendWithClasses = Backbone.Model.extend();

    /**
    * Collections
    */

    var FriendsCollection = Backbone.Collection.extend({
        model: FBfriend
    });

    var FriendsWithClassesCollection = Backbone.Collection.extend({
        model: FBfriendWithClasses
    });

    /**
    * Views Helpers
    */

    // TODO - These helpers are very similar to the ones used in calendar.js, maybe we can use 
    //  some abstraction to avoid code replication

    var ClassSingleViewHelpers = {
        getDateString: function (dateString) {
            return EQ.Helpers.dateTime.convertDateToString(dateString);
        },
        getInstructors: function (instructors) {
            var instructorsString;

            if (instructors.length) {
                _.each(instructors, function (instructor) {
                    instructorsString = instructor.instructor.firstName + ' ' + instructor.instructor.lastName;
                });
            }

            return instructorsString;
        },
        getEventDuration: function (eventDetail) {
            return EQ.Helpers.dateTime.getTimeRange(eventDetail.startDate, eventDetail.endDate);
        }
    };

    /**
    * Views
    */

    // No Friends View

    var NoFriendsView = Backbone.View.extend({
        el: 'section.no-friends',
        render: function () {
            this.$el.removeClass('is-hidden');
        }
    });

    // Friends on EQ Views

    var FriendsOnEQView = Backbone.View.extend({
        el: 'section.friends-whithout-classes',
        events: {
            'click .see-more': 'seeMore',
            'click .icon-right-arrow': 'goToNext',
            'click .icon-left-arrow': 'goToPrev'
        },
        goToNext: function (e) {
            e.preventDefault();
            var $carousel = this.$el.find('.fb-friends-list .friends-carousel');
            $carousel.data('owlCarousel').next();
        },
        goToPrev: function (e) {
            e.preventDefault();
            var $carousel = this.$el.find('.fb-friends-list .friends-carousel');
            $carousel.data('owlCarousel').prev();
        },
        seeMore: function (e) {
            e.preventDefault();
            debug('[SEE MORE TRIGGERED]');
        },
        render: function () {
            var $carousel = this.$el.find('.fb-friends-list .friends-carousel');
            this.collection.each(function (fbFriend) {
                var friendsOnEQSimpleView = new FriendsOnEQSimpleView({ model: fbFriend });
                $carousel.append(friendsOnEQSimpleView.render().el);
            }, this);

            if (this.collection.length === 1) {
                this.$el.find('.icon-right-arrow').addClass('hidden');
            }

            this.$el.removeClass('is-hidden');

            // Init carousel
            App.loadComponent('owl-slider', $carousel, {
                singleItem: false,
                items: 3,
                itemsDesktop: [1200, 3],
                itemsTablet: [1023, 2],
                itemsMobile: [768, 1]
            });

            return this;
        }
    });

    var FriendsOnEQSimpleView = Backbone.View.extend({
        tagName: 'ul',
        template: _.template($('#fbFriendTemplate').html()),
        render: function () {
            this.$el.html(this.template(this.model.toJSON()));
            return this;
        }
    });

    // Friends with Classes Views

    var FriendsWithClassesView = Backbone.View.extend({
        el: 'section.many-friends-classes',
        events: {
            'click .choose-friends': 'chooseFriends',
            'click .icon-right-arrow': 'goToNext',
            'click .icon-left-arrow': 'goToPrev'
        },
        goToNext: function (e) {
            e.preventDefault();
            var $carousel = this.$el.find('.fb-modules-container');
            $carousel.data('owlCarousel').next();
        },
        goToPrev: function (e) {
            e.preventDefault();
            var $carousel = this.$el.find('.fb-modules-container');
            $carousel.data('owlCarousel').prev();
        },
        chooseFriends: function (e) {
            e.preventDefault();
            debug('[CHOOSE FRIENDS TRIGGERED]');
        },
        showHideArrows: function () {
            var $carousel = this.$el.find('.fb-modules-container');

            if ($carousel.data('owlCarousel').itemsAmount > $carousel.data('owlCarousel').visibleItems.length) {
                this.$el.find('.navigation').removeClass('hidden');
            } else {
                this.$el.find('.navigation').addClass('hidden');
            }
        },
        render: function () {
            var $carousel = this.$el.find('.fb-modules-container'),
                self = this;

            this.collection.each(function (fbFriendWithClasses) {
                var friendsWithClassesSimpleView = new FriendsWithClassesSimpleView({ model: fbFriendWithClasses });
                $carousel.append(friendsWithClassesSimpleView.render().el);
            }, this);

            if (this.collection.length === 1) {
                this.$el.find('.icon-right-arrow').addClass('hidden');
            }

            // Init carousel
            App.loadComponent('owl-slider', $carousel, {
                singleItem: false,
                items: 3,
                itemsDesktop: [1200, 3],
                itemsTablet: [1023, 2],
                itemsMobile: [768, 1],
                autoHeight: false,
                afterInit: function () {
                    setTimeout(function () {
                        self.showHideArrows();
                    }, 2000);
                },
                afterUpdate: function () {
                    self.showHideArrows();
                }
            });

            this.$el.removeClass('is-hidden');
            return this;
        }
    });

    var OneFriendWithClassesView = Backbone.View.extend({
        el: 'section.one-friend-class',
        events: {
            'click .choose-friends': 'chooseFriends'
        },
        chooseFriends: function () {
            debug('[CHOOSE FRIENDS TRIGGERED]');
        },
        render: function () {
            this.collection.each(function (fbFriendWithClasses) {
                var friendsWithClassesSimpleView = new FriendsWithClassesSimpleView({ model: fbFriendWithClasses });
                this.$el.find('.left-container').append(friendsWithClassesSimpleView.render().el);
            }, this);

            this.$el.removeClass('is-hidden');
            return this;
        }
    });

    var FriendsWithClassesSimpleView = Backbone.View.extend({
        tagName: 'div',
        className: 'fb-class-container',
        template: _.template($('#fbFriendWithClassesTemplate').html()),
        events: {
            'click a.add-class': 'addClass'
        },
        getRenderData: function () {
            var data = {};
            data = this.model.toJSON();
            return _.extend(data, ClassSingleViewHelpers);
        },
        buildLoader: function () {
            var loaderAndError;

            if (this.loaderAndError) {
                return this.loaderAndError;
            } else {
                loaderAndError = EQ.Helpers.loaderAndErrorHandler(this.$el, {
                    type: 'overlay',
                    color: 'black'
                });

                this.loaderAndError = loaderAndError;
                return loaderAndError;
            }
        },
        addClass: function (e) {
            e.preventDefault();
            var classInfo = this.model.get('classInfo'),
                ENDPOINT,
                that = this;

            debug('AddClass', classInfo);

            ENDPOINT = APIEndpoint + '/me/calendar/' + classInfo.classInstanceId + '?isRecurring=false';

            var loaderAndError = that.buildLoader();

            loaderAndError.showLoader();

            $.ajax({
                type: 'POST',
                url: ENDPOINT,
                contentType: 'application/json',
                xhrFields: { 'withCredentials': true },
                dataType: 'json',
                success: function (data) {
                    debug('[ADDCLASS OK]', data);
                    loaderAndError.hideLoader();

                    that.$el.find('a.add-class').remove();
                },
                error: function (d) {
                    debug('server error', d.responseJSON);
                    loaderAndError.showError();

                    $('.active .class-overlay').addClass('active').find('p').html('That class is already on your calendar');
                }
            });
        },
        render: function () {
            this.$el.html(this.template(this.getRenderData()));
            return this;
        }
    });

    /**
    * Component Init.
    */

    var FacebookClasses = {};

    FacebookClasses.init = function ($el) {
        debug('[Facebook Classes] ', FacebookClasses);
        var $connectSection = $el.find('section.fb-friends-container'),
            $connectButton = $connectSection.find('.button');

        if (window.userProfileJson.FacebookId !== null) {
            var endDate = moment().add('weeks', 1),
                userInfo = {
                    'maxNumberOfRecords': 10,
                    'startDate': moment().format(),
                    'endDate': endDate.format()
                };
            this.getSocialActivitiesByUserInfo({ userInfo: userInfo }, $connectSection, $connectButton);
        }
        else {
            $connectButton.on('click', function (e) {
                e.preventDefault();
                // $connectSection.addClass('hidden');
                FacebookClasses.connect($connectSection, $connectButton);
            });
        }

    };
    /* global FB */
    FacebookClasses.getToken = function (cb) {
        FB.login(function (response) {
            if (response.status === 'connected') {
                cb(response.authResponse.accessToken);
            }
        }, {
            scope: 'email,user_likes'
        });
    };

    FacebookClasses.connect = function ($connectSection, $connectButton) {
        // TODO: show spinning wheel while requesting.

        if (window.userProfileJson.FacebookId !== null) {
            var endDate = moment().add('weeks', 1),
            userInfo = {
                'maxNumberOfRecords': 10,
                'startDate': moment().format(),
                'endDate': endDate.format()
            };
            this.getSocialActivitiesByUserInfo({ userInfo: userInfo }, $connectSection, $connectButton);
        }
        else {
            var self = this;
            this.getToken(function (token) {
                debug('FBT:', token);
                var endDate = moment().add('weeks', 1),
                userInfo = {
                    'facebookAccessToken': token,
                    'maxNumberOfRecords': 10,
                    'startDate': moment().format(),
                    'endDate': endDate.format()
                };
                self.getSocialActivitiesByUserInfo({ userInfo: userInfo }, $connectSection, $connectButton);
            });
        }

    };

    FacebookClasses.prepareDataForUsersWitClasses = function (rawClassesData) {
        var friendsWithClasses = [];
        _.each(rawClassesData, function (rawClass) {
            if (rawClass.friends && rawClass.friends.length > 0) {
                // Take the first friend
                var friend = rawClass.friends[0];
                // get the amount of 'others' friends with this class also
                friend.others = rawClass.friends.length - 1;
                // Clear friends from classinfo
                delete rawClass.friends;
                friend.classInfo = rawClass;

                friendsWithClasses.push(friend);
            }
        });
        return friendsWithClasses;
    };

    FacebookClasses.getSocialActivitiesByUserInfo = function (dataUserInfo, $connectSection, $connectButton) {
        var loaderAndError = EQ.Helpers.loaderAndErrorHandler($connectButton, {
            type: 'button',
            errorTitle: 'Error',
            color: 'black'
        });

        loaderAndError.showLoader();
        $.ajax(APIEndpoint + '/me/calendar/social-activities', {
            data: JSON.stringify(dataUserInfo.userInfo),
            contentType: 'application/json',
            type: 'POST',
            xhrFields: {
                withCredentials: true
            }
        })
        .done(function (data) {
            debug('SOCIAL ACTIVITIES', data);

            $connectSection.addClass('hidden');
            loaderAndError.hideLoader();

            // Mock data to emulate friends with classes
            // data = {'fbFriendsOnEQ': {'fbFriends': [], 'classesWithFriends': [{'friends': [{'userId': 290753, 'displayName': 'Eqx Tesst', 'firstName': 'Eqx', 'lastName': null, 'homeFacilityName': '17th St', 'city': 'astoria', 'profileId': '100002691080894', 'profileImageUrl': 'http: //graph.facebook.com/100002691080894/picture'}], 'facility': {'facilityId': '116', 'clubId': 104, 'name': 'Equinox 17th Street', 'shortName': '17th', 'mobileName': '17th', 'webName': '17th St', 'timeZoneId': 'Eastern Standard Time', 'regionId': null, 'isPresale': false}, 'classId': 3595, 'classInstanceId': 4623523, 'name': 'Barre Burn', 'instructors': [{'instructor': {'id': 4779, 'firstName': 'Alicia', 'lastName': 'Archer', 'bio': ''}, 'substitute': null}], 'startDate': '2014-06-04T10:30:00Z', 'startLocal': '2014-06-04T07:30:00', 'endDate': '2014-06-04T11:30:00Z', 'endLocal': '2014-06-04T08:30:00', 'timeSlot': 'Morning', 'primaryCategory': {'categoryId': '4', 'categoryName': 'Barre', 'iconImageUrl': 'http: //local-api.equinox.com/cms/images/48959c7d-c4d0-42d1-932e-078ad5bdc41d/en-us/barre.png?edf0a29e1ff76', 'bannerImageUrl': 'http: //local-api.equinox.com/cms/images/c8e73ee4-aaae-4384-8481-48758a2afe17/en-us/class-detail-header-desktop.jpg?edfb5ad8b06aa'}, 'additionalCategories': null, 'isOnCalendar': false, 'userEventId': null, 'status': {'isWithinBookingWindow': false, 'isFull': false, 'bikesLeft': 0, 'hasReservation': false, 'localId': null}, 'isBookableOnline': false, 'displayTime': '07:30 AM - 08:30 AM EDT', 'isBookable': false, 'isHappeningNow': false, 'isFinished': false, 'facilityCurrentDateTime': '2014-06-03T18:42:14.0257376'}, {'friends': [{'userId': 290753, 'displayName': 'Eqx Tesst', 'firstName': 'Eqx', 'lastName': null, 'homeFacilityName': '17th St', 'city': 'astoria', 'profileId': '100002691080894', 'profileImageUrl': 'http: //graph.facebook.com/100002691080894/picture'}], 'facility': {'facilityId': '871', 'clubId': 189, 'name': 'Equinox Kensington', 'shortName': 'Ken', 'mobileName': 'Ken', 'webName': 'Kensington', 'timeZoneId': 'GMT Standard Time', 'regionId': null, 'isPresale': false}, 'classId': 2290, 'classInstanceId': 4638540, 'name': 'Chisel', 'instructors': [{'instructor': {'id': 5281, 'firstName': 'Geoff', 'lastName': 'Bagshaw', 'bio': 'Geoff Bagshaw,  one of Canada’s most energetic and inspirational fitness leaders,  began his fitness career in Vancouver over 25 years ago. To date,  Geoff has presented workshops and taught in 16 countries around the world,  reaching more than 500, 000 people. He’s been featured in 10 fitness workout and training DVDs and regularly appears in the media as a fitness expert for local and national magazines and TV programs. Geoff was twice honoured with the title of “Canadian Fitness Presenter of the Year” by Can-Fit-Pro,  Canada’s largest fitness association. He joined Equinox in 2010 and spent 4 years in Miami as Group Fitness Manager at the Aventura and South Beach clubs and as Area Group Fitness Manager for Florida. Geoff currently resides in London,  where he is now in the role of Area Group Fitness Manager for Equinoxs UK Operations.'}, 'substitute': null}], 'startDate': '2014-06-04T12:30:00Z', 'startLocal': '2014-06-04T09:30:00', 'endDate': '2014-06-04T13:30:00Z', 'endLocal': '2014-06-04T10:30:00', 'timeSlot': 'Morning', 'primaryCategory': {'categoryId': '3', 'categoryName': 'Conditioning', 'iconImageUrl': 'http: //local-api.equinox.com/cms/images/d82f259c-45cd-4c28-b9a3-91cb52bd56be/en-us/conditioning.png?edf0a2944d321', 'bannerImageUrl': 'http: //local-api.equinox.com/cms/images/c8e73ee4-aaae-4384-8481-48758a2afe17/en-us/class-detail-header-desktop.jpg?edfb5ad8b06aa'}, 'additionalCategories': null, 'isOnCalendar': false, 'userEventId': null, 'status': {'isWithinBookingWindow': false, 'isFull': false, 'bikesLeft': 0, 'hasReservation': false, 'localId': null}, 'isBookableOnline': false, 'displayTime': '09:30 AM - 10:30 AM BST', 'isBookable': false, 'isHappeningNow': false, 'isFinished': false, 'facilityCurrentDateTime': '2014-06-03T23: 42: 14.0257376'}], 'hasFriends': true}};

            if (data.fbFriendsOnEQ.hasFriends === false) {
                //Init noFriendsView
                var noFriendsView = new NoFriendsView();
                noFriendsView.render();
            } else if (data.fbFriendsOnEQ.classesWithFriends.length > 1) {
                //Init FriendsOnEQView
                var usersWithClassesData,
                    friendsWithClassesCollection,
                    friendsWithClassesView;

                usersWithClassesData = FacebookClasses.prepareDataForUsersWitClasses(data.fbFriendsOnEQ.classesWithFriends);

                friendsWithClassesCollection = new FriendsWithClassesCollection(usersWithClassesData);

                if (usersWithClassesData.length > 1) {
                    friendsWithClassesView = new FriendsWithClassesView({ collection: friendsWithClassesCollection });
                } else {
                    friendsWithClassesView = new OneFriendWithClassesView({ collection: friendsWithClassesCollection });
                }

                friendsWithClassesView.render();
            } else {
                // Friends without Classes
                var friendsCollection = new FriendsCollection(data.fbFriendsOnEQ.fbFriends),
                    friendsOnEQView = new FriendsOnEQView({ collection: friendsCollection });

                friendsOnEQView.render();
            }
        })
        .fail(function (data) {
            var failedResponse = data.responseJSON;
            var errorMessage = failedResponse.error ? failedResponse.error.message : failedResponse.message;
            if (failedResponse.error && failedResponse.error.messageId === 50019) {
                FB.api('/me', function (profileResponse) { // this line throws error
                    /* jshint camelcase:false */
                    global.location.href = '/login/connect?firstName=' + profileResponse.first_name + '&lastName=' + profileResponse.last_name;
                    /* jshint camelcase:true */
                });

            } else {
                $('.error-title').text(errorMessage);
            }
            debug('Server Error');
            loaderAndError.showError();
        });
    };

    App.Components['social-activity'] = function ($el) {
        FacebookClasses.init($el);
    };

}(window, window.App));