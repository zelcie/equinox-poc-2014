(function (App) {
    'use strict';

    /* global EQ, allRegionsData */

    var ClubsMap = function ($el, options) {
        this.$el = $el;
        this.options = options;

        EQ.Maps.Load($.proxy(this.init, this));
    };

    ClubsMap.prototype = {
        init: function () {
            // FIXME: find a better way of modularizing instead of overwriting other page's variables.
            this.Club = App.Pages.Clubs.Club;
            // TODO: this will break if clubsMap component is used within Club Locator module.
            this.Map = App.Pages.Clubs.Map = new EQ.Maps.Map(this.$el[0], 0, 0, this.options);


            var that = this;

            // if set center is trigger, zoom out to the US map here
            EQ.Maps.on('CLUB_SET_CENTER', function (loc) {
                that.Map.map.setCenter({lat: loc.lat, lng: loc.lng});
                that.Map.map.minZoom = 3; // somehow zoom wont work without this
                //that.Map.unfreeze(); // somehow zoom wont work without this

                // putting setZoom in a timeout is the only way i can get
                // this fucker to act right.
                setTimeout(function () {
                    that.Map.map.setZoom(1); //that.Map.markers.markers,
                }, 1000);
            });

            this.Map.mobilePanning();

            if (typeof allRegionsData === 'undefined') {
                throw new Error('Regions Data for Clubs Map is not defined.');
            }

            this.parseRegionsData();

            // if (typeof selectedRegion !== 'undefined') {
            //     this.select(selectedRegion);
            // }

        },
        parseRegionsData: function (newData) {
            var clubsMap = this;

            clubsMap.regions = {};
            clubsMap.facilities = [];

            if (newData) {
                EQ.Helpers.fixRegionProperty(newData);
                clubsMap.Map.markers.empty();
            }

            // Store all facilities worldwide
            $.each(newData || allRegionsData, function (i, region) {
                clubsMap.facilities = clubsMap.facilities.concat(EQ.Helpers.getAllFacilities(region));
                clubsMap.regions[region.ShortName] = region;
            });

            // Add all facilities worldwide
            $.each(clubsMap.facilities, function (i, club) {
                clubsMap.Club.parse(club);
            });
        },
        select: function (regionName, clubId) {
            if (regionName) {
                var facilities = EQ.Helpers.getAllFacilities(this.regions[regionName]);

                if (!facilities) {
                    throw new Error('There is no data for the region selected.');
                }

                this.Map.fit(facilities);
            } else if (clubId) {
                var facility = this.getFacilityByClubId(clubId);
                this.Map.fit([facility]);
            }
        },
        getFacilityByClubId: function (id) {
            var candidate = null;
            $.each(this.facilities, function (i, facility) {
                if (facility.ClubID === id) {
                    candidate = facility;
                }
            });

            return candidate;
        },
        getFacilityById: function (id) {
            var candidate = null;
            $.each(this.facilities, function (i, facility) {
                if (facility.Id === id) {
                    candidate = facility;
                }
            });

            return candidate;
        }
    };

    App.Components['clubs-map'] = function ($el, options) {
        if ($el.data('clubsMap')) {
            return $el.data('clubsMap');
        }
        $el.data('clubsMap', new ClubsMap($el, options));
        console.log('trigger');
        // Use this event if you want to access the .data('clubsMap') outside of this.
        App.Events.trigger('clubs-map:loaded');
    };

} (window.App));