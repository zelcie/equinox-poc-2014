(function (App) {
    'use strict';

    /* global EQ, allRegionsData, debug */

    var selectedRegion;

    var Handlers = {
        classes: {
            /**
             * Init
             *
             * This method populates the regions drop down with all the region data and then calls to populate all facilities for that region. If
             * the user is on the /Clubs page, find near clubs. For /region/ pages don't as we will be showing the region map instead
             *
             * @param module
             */
            init: function () {
                console.warn('nearme > classes > init');
                // show user location map only on clubs page
                if (!$('.page').hasClass('clubs-region')) {
                    EQ.Geo.getNearestClub(function (club) {
                        debug('[NEARME] Classes closest club:', club);
                        EQ.Maps.trigger('CLUB_MARKER_CLICK', {
                            facility: club
                        });

                        EQ.Maps.fixLayout();
                    });
                }
            }
        },
        spa: {
            init: function (module) {
                console.warn('nearme > spa > init');
                module.$el.hide();

                $.get('/esb/getnearestspas/10018?radius=10000&numResults=1000', function (data) {
                    var regionsMarkup = '',
                        regions = module._regions,
                        allFacilities = [];

                    module.clubsMap.parseRegionsData(data);

                    $.each(data, function (i, region) {
                        // FIXME: the next line should be binded in backend.
                        region.ShortName = region.ShortName.replace(/ /g, '-');

                        var regionFacilities = EQ.Helpers.getAllFacilities(region);

                        allFacilities = allFacilities.concat(regionFacilities);

                        if (regionFacilities.length) {
                            // Append region options
                            regionsMarkup += '<option value="' +
                                            region.ShortName +
                                            (selectedRegion === region.ShortName ? '" selected="selected' : '') +
                                            '">' +
                                            region.Name +
                                            '</option>';
                            regions[region.ShortName] = region;
                        }
                    });

                    module.$regions.html(regionsMarkup);

                    // set a default value
                    if (selectedRegion) {
                        module.$regions.next('span.option')
                                .hide()
                                .text(module.$regions.find('option:selected').text())
                                .css('display', 'inline-block');
                    }

                    module.updateFacilities();

                    module.$el.show();

                    debug('[NEARME] Looking up closest spa within', allFacilities.length, 'facilities.');

                    EQ.Geo.getLatLng(function (data) {
                        if (data && data.coords) {
                            var club = EQ.Helpers.getClosestWithin(allFacilities, {
                                Latitude: data.coords.latitude,
                                Longitude: data.coords.longitude
                            });

                            debug('[NEARME] Spa closest club:', club);
                            EQ.Maps.trigger('CLUB_MARKER_CLICK', {
                                facility: club
                            });

                            EQ.Maps.fixLayout();
                        }
                    });
                });
            }
        }
    };

    var NearMe = function ($el, type) {

        if (!Handlers[type]) {
            throw new Error('No handler for nearme module type ' + type);
        }

        this.$el = $el;
        this.$regions = $el.find('select[name="regions"]');
        this.$facilities = $el.find('select[name="facilities"]');
        this._regions = {};
        this.type = Handlers[type];

        this.bind();
        this.load();
    };

    NearMe.prototype = {
        bind: function () {
            var that = this;

            this.$facilities.on('change', function () {
                that.changeFacility($(this).val());
            });

            EQ.Maps.on('CLUB_MARKER_CLICK', function (data) {
                var region = EQ.Helpers.getRegionByTitle(data.facility.Region),
                    clubId = data.facility.ClubID;

                if (region && clubId) {
                    if (that.$regions.val() !== region.ShortName) {
                        that.$regions
                            .prop('selectedIndex', that.$regions.find('option[value="' + region.ShortName + '"]').index())
                            .trigger('change');
                    }

                    if (that.$facilities.val() !== clubId) {
                        that.$facilities
                            .prop('selectedIndex', that.$facilities.find('option[value="' + clubId + '"]').index())
                            .trigger('change');
                    }
                }
            });

            $('.map-container').on('click', '.map-interaction-lock a .icon-refresh', function (evt) {
                evt.preventDefault();

                var val = $('.select-wrapper').find('select[name="facilities"]').val();

                that.changeFacility(val);


                /*var club = {
                    Address: '54 Murray Street',
                    ClubID: '130',
                    ClubName: 'Tribeca',
                    Country: '',
                    Description: 'Equinox Tribeca',
                    GoogleAddress: '54 Murray Street, New York, NY 10007',
                    Id: '111',
                    Latitude: '40.714182',
                    Longitude: '-74.009723',
                    Region: 'New York',
                    SchedulePDF: 'http://www.equinox.com/admin/groupfitness/pdf/getschedule.ashx?clubid=130',
                    ShortName: 'Tribeca',
                    Telephone: '212.566.6555',
                    URL: 'http://local-mobile.equinox.com/clubs/new-york/downtown/tribeca'
                };

                //that.loadNearClubsMap();
                EQ.Maps.trigger('CLUB_MARKER_CLICK', {
                    facility: club,
                    marker: this
                });

                EQ.Maps.fixLayout();*/
            });
        },

        populateRegionsDropdown: function (module, opts) {
            var regionsMarkup = '',
                regions = module._regions;

            var options = opts;

            if (options.defaultText) {
                regionsMarkup += '<option selected="selected" value="' + options.defaultValue + '">' + options.defaultText + '</option>';
            }

            $.each(allRegionsData, function (i, region) {
                if (EQ.Helpers.getAllFacilities(region).length) {
                    // Append region options
                    regionsMarkup += '<option value="' +
                        region.ShortName +
                        (selectedRegion === region.ShortName ? '" selected="selected' : '') +
                        '">' +
                        region.Name +
                        '</option>';
                    regions[region.ShortName] = region;
                }
            });

            module.$regions.html(regionsMarkup);

            // set a default value
            if (selectedRegion) {
                // in case there is more than one instance of the select
                // drop down in the DOM
                for (var i = 0; i < module.$regions.length; i++) {
                    $(module.$regions[i]).next('span.option')
                        .hide()
                        .text($(module.$regions[i]).find('option:selected').text())
                        .css('display', 'inline-block');
                }
            }
        },

        loadDropDowns: function () {
            var that = this;
            // get the regions in an object $.fn.selectWrapper() would understand
            var regions = EQ.Helpers.normalizeRegionsData();

            // add the select region default value to the drop down
            regions.unshift({
                value: 'Select-Region',
                text: 'Select Region',
                selected: true
            });

            that._regions = EQ.Helpers.getRegions();

            $('.select-regions').selectWrapper({
                data: regions,
                syncInstances : true,
                onChange: function ($el) {
                    var selectedValue = $el.find('option:selected').val();
                    var selectedText = $el.find('option:selected').text();


                    that.updateFacilities($el);

                    if ($('.page').attr('data-region') === 'undefined') {
                        that.loadRegionInfo(selectedValue, selectedText);
                    }
                },
                onReady: function ($el) {
                    that.updateFacilities($el);
                },
                defaultValue : ($('.page').attr('data-region')) ? $('.page').attr('data-region') : null
            });

            that.loadNearClubsMap();
        },

        setDefaultMap: function () {
            var that = this;

            EQ.Maps.Map(that.$el.find('.map-container'), 41.850033, -87.6500523, {
                zoom: 3,
                minZoom: 3
            });
        },

        /**
         * setDefault
         *
         * In case the user has not chosen to allow or deny, set the map to show
         * the U.S. map and some default option in the dropdown
         */
        setDefault: function () {
            EQ.Maps.trigger('CLUB_SET_CENTER', {
                lat: 41.850033,
                lng: -87.6500523
            });
            selectedRegion = 'Select Region'; // val() to apply to the selected option
            this.populateRegionsDropdown(this, {defaultText: 'Select Region', defaultValue: 'Select-Region'});
        },

        load: function () {
            var that = this;

            this.loadDropDowns();

            EQ.Maps.Load(function () {
                that.setDefault();
            });

            EQ.Maps.on('IDLE_LOAD', function () {
                EQ.Geo.getNearestRegion(function (region) {
                    selectedRegion = region;
                    that.type.init(that); //ends up calling Handlers.classes.init()
                }, function () {
                    that.setDefault();
                });
            });
        },

        show: function (club) {
            var $club = $('.club-information');
            $club.find('[data-club-href="searchclasses"]').attr('href', '/classes/search?clubs=' + club.Id);

            console.warn('nearme > on show');
            $.each(club, function (key, value) {
                if (typeof key === 'string') {
                    key = key.toLowerCase();
                    $club.find('[data-club-text="' + key + '"]').text(value);
                    if (key === 'telephone') {
                        var $phoneItem = $club.find('[data-club-text="' + key + '"]');
                        //from . to -
                        var formatedValue = value.replace(/\./g, '');
                        var phoneLink = 'tel:' + formatedValue;
                        $phoneItem.attr('href', phoneLink);
                    }
                    $club.find('[data-club-href="' + key + '"]').attr('href', value);
                }
            });

            $club.removeClass('hidden');
        },

        changeFacility: function (clubId) {
            if (!this.clubsMap) {
                this.clubsMap = $('.map-container').data('clubsMap');
            }

            var facility = this.clubsMap.getFacilityByClubId(clubId);

            if (facility) {
                if ($('.map-container').find('.gm-style').length > 1) {
                    $('.map-container').find('.gm-style:nth-child(2)').remove();
                }
                this.clubsMap.Map.fit([facility], -1);
                this.show(facility);
            }
        },

        /**
         * loadNearClubsMap
         *
         * Does a Geolocation check for the nearest club and then triggers a click
         * on the maps with this club object as the facility in focus. It then trigger
         * the fixLayout method to fix any flakiness on the re-rendering
         *
         * Sample club object:
         *        var club = {
         *           Address: '54 Murray Street',
         *           ClubID: '130',
         *           ClubName: 'Tribeca',
         *           Country: '',
         *           Description: 'Equinox Tribeca',
         *           GoogleAddress: '54 Murray Street, New York, NY 10007',
         *           Id: '111',
         *           Latitude: '40.714182',
         *           Longitude: '-74.009723',
         *           Region: 'New York',
         *           SchedulePDF: 'http://www.equinox.com/admin/groupfitness/pdf/getschedule.ashx?clubid=130',
         *           ShortName: 'Tribeca',
         *           Telephone: '212.566.6555',
         *           URL: 'http://local-mobile.equinox.com/clubs/new-york/downtown/tribeca'
         *       }
         *
         * @param clubId
         */
        loadNearClubsMap: function () {
            EQ.Geo.getNearestClub(function (club) {
                debug('[NEARME] Classes closest club:', club);

                EQ.Maps.trigger('CLUB_MARKER_CLICK', {
                    facility: club,
                    marker: this
                });

                EQ.Maps.fixLayout();
            });
        },

        /**
         * updateFacilities
         *
         * Populates the `Club Facilities` drop down(s) with all the clubs in that region/area
         *
         * @param el it's safer to pass $(this) object from the onchange to target only this instance
         */
        updateFacilities: function (el) {
            // if el is passed, use that instead as there might be multiple instances of the drop down
            this.$regions = el || this.$regions;

            var facilitiesMarkup = '',
                region = this._regions[this.$regions.val()],
                facilities;

            if (!region) {
                console.warn('No region for selected value:', this.$regions.val());
                return;
            }

            facilities = EQ.Helpers.getAllFacilities(region);

            $.each(facilities, function (i, club) {
                facilitiesMarkup += '<option value="' + club.ClubID + '">' + club.ClubName + '</option>';

                EQ.Helpers.setPositionGetter(club);
            });

            this.$facilities.html(facilitiesMarkup);

            var defaultText = $(this.$facilities[0]).find('option:selected').text();
            var defaultValue = $(this.$facilities[0]).find('option:selected').val();

            this.$facilities.val(defaultValue);

            var that = this;
            setTimeout(function () {
                that.$facilities.trigger('change');
            }, 3000);

            this.$facilities.next('span.option').text(defaultText).css('width', '100%');

        },

        loadRegionInfo: function (selectedValue, selectedText) {
            if (selectedValue !== 'Select-Region') {
                debug('[NEARME] loading region: ' + selectedValue);
                $('.region-information .title').text(selectedText);
                $('.region-information .description').load('/regions/' + selectedValue + ' .paragraph p');
                $('.region-information').slideDown();
            }
        }
    };

    App.Components.nearme = function ($el, type) {
        if ($el.data('nearme')) {
            return $el.data('nearme');
        }

        $el.data('nearme', new NearMe($el, type));
    };


} (window.App));