(function (App) {
    'use strict';

    /* global APIEndpoint, allRegionsData, Backbone, debug, _, moment, EQ */

    App.Components['classes-list'] = function ($el) {
        var ClassesList = {};

        ClassesList.currentXHR = null;
        ClassesList.currentFilters = null;
        ClassesList.currentDate = moment();

        //Omniture Tag
        window.tagData = window.tagData || {};
        window.tagData.search = window.tagData.search || {};
        window.tagData.search.type = 'class';

        // Dropdowns Toggle
        $('.classes-list .toggle-day').on('click', function (e) {
            e.preventDefault();
            $(this).parent().toggleClass('active');
            $(this).find('span').toggleClass('icon-right-arrow icon-dropdown');
        });

        // Selected day fetch
        Backbone.Events.on('dayfilter:click', function (e) {
            e.preventDefault();
            var day = $(e.currentTarget).data('date');
            if (ClassesList.currentDate !== day) {
                Backbone.Events.trigger('classes-list:fetch', null, day);
            }
        });

        // Week change fetch
        Backbone.Events.on('dayfilter:prev-week', function () {
            var week = moment(ClassesList.currentDate).subtract(1, 'week');
            Backbone.Events.trigger('classes-list:fetch', null, week);
        });
        Backbone.Events.on('dayfilter:next-week', function () {
            var week = moment(ClassesList.currentDate).add(1, 'week');
            Backbone.Events.trigger('classes-list:fetch', null, week);
        });

        // Day change fetch
        Backbone.Events.on('dayfilter:prev-day', function () {
            var day = moment(ClassesList.currentDate).subtract(1, 'day');
            Backbone.Events.trigger('classes-list:fetch', null, day);
        });
        Backbone.Events.on('dayfilter:next-day', function () {
            var day = moment(ClassesList.currentDate).add(1, 'day');
            Backbone.Events.trigger('classes-list:fetch', null, day);
        });

        // Switch Days on mobile
        Backbone.Events.on('dayfilter-mobile:click', function (e) {
            e.preventDefault();

            var day = $(e.currentTarget).data('index');

            // Change current indicator
            $('.calendar-list-container').find('.mobile-first').removeClass('mobile-first');
            $('.calendar-list-container').find('.mobile-current').removeClass('mobile-current');
            $(e.currentTarget).addClass('mobile-current');

            // Show/Hide Days
            $el.find('.classes-day.current').removeClass('current');
            $el.find('[data-day="' + day + '"]').addClass('current');
        });

        // Models
        var ClassModel = Backbone.Model.extend({});

        // Views
        var DatesHeaderView = Backbone.View.extend({
            el: $('.classes-list .day-dates'),
            initialize: function (options) {
                // Attach options to the view.
                this.options = {};
                _.extend(this.options, options);
            },
            render: function () {
                this.$el.html('');
                //Add week
                for (var i = 0; i <= 6; i++) {
                    var day = moment(this.options.startDate);

                    day.add('days', i);

                    var name = moment(day).format('ddd'),
                        month = moment(day).format('MMM'),
                        number = moment(day).format('D'),
                        isToday = moment(day).isSame(moment(), 'day') ? 'current-day' : '',
                        isSelected = moment(day).isSame(this.options.startDate, 'day') ? 'current-day' : '';

                    console.log(isToday);

                    var calendarDay = {
                        'name': name,
                        'date': month + ' ' + number,
                        'isToday': isToday,
                        'isSelected': isSelected
                    };

                    if (isToday) {
                        calendarDay.name = 'Today';
                    }

                    // Set data-date for the dates containers.
                    $el.find('[data-day="' + (i + 1) + '"]').attr('data-date', moment(day).format('YYYY-MM-DD'));

                    // Append days headers.
                    var dayHeaderTpl = _.template($('#dayHeaderTemplate').html());
                    this.$el.append(dayHeaderTpl(calendarDay));
                }
            }
        });

        var EmptyResultsView = Backbone.View.extend({
            template: _.template($('#emptyResult').html()),
            events: {
                'click .clear-filters': 'clearFilters'
            },
            clearFilters: function (e) {
                e.preventDefault();
                Backbone.Events.trigger('classes-list:remove-filters');
                Backbone.Events.trigger('classes-list:fetch', null);
            },
            initialize: function () {
                this.listenTo(Backbone.Events, 'classes-list:fetch', this.remove);
            },
            render: function () {
                this.setElement(this.template({}));
                return this;
            }
        });

        var EmptyFilterView = Backbone.View.extend({
            template: _.template($('#emptyFilterResult').html()),
            initialize: function () {
                this.listenTo(Backbone.Events, 'classes-list:fetch', this.remove);
            },
            render: function () {
                this.setElement(this.template({}));
                return this;
            }
        });

        var EmptyDayView = Backbone.View.extend({
            template: _.template($('#emptyDay').html()),
            initialize: function () {
                this.listenTo(Backbone.Events, 'classes-list:fetch', this.remove);
            },
            render: function () {
                this.setElement(this.template({}));
                return this;
            }
        });

        var DayView = Backbone.View.extend({
            template: _.template($('#classTemplate').html()),
            events: {
                'click .add-to-calendar': 'addClass'
            },
            'addClass': function (e) {
                e.preventDefault();
                var self = this,
                    ENDPOINT = APIEndpoint + '/me/calendar/' + this.model.get('classInstanceId') + '?isRecurring=false',
                    loaderAndError,
                    $link = self.$el.find('a.add'),
                    $message = self.$el.find('.message');

                // Loader init
                loaderAndError = EQ.Helpers.loaderAndErrorHandler(self.$el.find('.add'), {
                    type: 'button'
                });
                loaderAndError.showLoader();

                $.ajax({
                    type: 'POST',
                    url: ENDPOINT,
                    contentType: 'application/json',
                    xhrFields: { 'withCredentials': true },
                    dataType: 'json',
                    success: function (data) {
                        debug('[ADDCLASS OK]', data);

                        loaderAndError.hideLoader();

                        $link.remove();
                        $message.removeClass('closed');
                        setTimeout(function () {
                            $message.addClass('closed');
                        }, 5000);

                        self.$el.find('.class-added').removeClass('hidden');

                        window.tagData.exportToCal = window.tagData.exportToCal || {};
                        window.tagData.exportToCal = {
                            'action': 'export-complete',
                            'type': 'class'
                        };
                        window.track('exportToCal', window.tagData.exportToCal);
                    },
                    error: function (d) {
                        loaderAndError.hideLoader();

                        if (d.status === 401) {
                            self.$el.find('.grey-popup').fadeIn().delay(5000).fadeOut();
                        } else {
                            debug('server error', d);
                            $link.remove();
                            $message.find('.copy').html('<p><strong></strong></p>').find('strong').text(d.responseJSON.error.message || d.responseJSON.message);
                            $message.addClass('bike').removeClass('closed');
                            setTimeout(function () {
                                $message.addClass('closed');
                            }, 5000);
                        }
                    }
                });
            },
            render: function () {
                var data = this.model.toJSON();
                this.setElement(this.template(data));
                return this;
            }
        });

        // Collections
        var ClassCollection = Backbone.Collection.extend({
            model: ClassModel,
            configRequest: function (options) {
                this.options = {};
                _.extend(this.options, options);
            },
            url: function () {
                var url = APIEndpoint + '/search/classes';
                return url;
            }
        });

        ClassesList.loaderAndError = EQ.Helpers.loaderAndErrorHandler($('.classes-list .loader-holder'), {
            color: 'black'
        });

        ClassesList.refresh = function (data, date) {
            // Dates Headers
            var datesHeaderView = new DatesHeaderView({
                    startDate: moment(date || new Date())
                }),
                $loader = $('.classes-list .loader-holder'),
                $classesListError = $('.classes-list .error-state');

            datesHeaderView.render();

            // Cancel ongoing request.
            if (ClassesList.currentXHR !== null) {
                ClassesList.currentXHR.abort();
            }

            var classCollection = new ClassCollection();
            window.tagData.search.results = classCollection ? classCollection.length.toString() : '0';

            // Configure class search request with filters
            classCollection.configRequest({
                'fromDate': moment(EQ.Helpers.dateTime.getCurrentDay(date || new Date()).startDate).startOf('day').format('YYYY-MM-DDTHH:mm:ss\\Z'),
                'toDate': moment(EQ.Helpers.dateTime.getCurrentDay(date || new Date()).startDate).add('week', 1).startOf('day').format('YYYY-MM-DDTHH:mm:ss\\Z'),
                'clubs': data.clubs || [],
                'instructors': data.instructors || [],
                'categories': data.categories || [],
                'classes': data.classes || []
            });

            // Loader & Errors
            $loader.removeClass('hidden');
            $classesListError.removeClass('hidden');
            $('.results-list').addClass('hidden');

            ClassesList.loaderAndError.hideError();
            ClassesList.loaderAndError.showLoader();


            // Fetch
            ClassesList.currentXHR = classCollection.fetch({
                'method': 'POST',
                'data': JSON.stringify({
                    'startDate': classCollection.options.fromDate,
                    'endDate': classCollection.options.toDate,
                    'facilityIds': classCollection.options.clubs || [],
                    'instructorIds': classCollection.options.instructors || [],
                    'classCategoryIDs': classCollection.options.categories || [],
                    'classIds': classCollection.options.classes,
                    'dateIsUtc': false
                }),
                'xhrFields': { 'withCredentials': true },
                'contentType': 'application/json',
                'dataType': 'json',
                'success': function (collection, response) {
                    ClassesList.loaderAndError.hideLoader();

                    if (response.classes.length === 0) {
                        $loader.addClass('hidden');
                        
                        var emptyView = new EmptyResultsView();
                        emptyView.listenTo(Backbone.Events, 'classes-list:remove-classes', emptyView.remove);
                        $classesListError.html(emptyView.render().el);
                    } else {
                        $('.results-list').removeClass('hidden');
                        $loader.addClass('hidden');
                        $classesListError.addClass('hidden');

                        response.classes.forEach(function (c) {
                            // Class Model
                            var classModel = new ClassModel(c);
                            // Create dayView
                            var dayView = new DayView({
                                model: classModel
                            });

                            dayView.listenTo(Backbone.Events, 'classes-list:remove-classes', dayView.remove);
                            $('[data-period="' + c.timeSlot.toLowerCase() + '"] [data-date="' + moment(c.startDate).format('YYYY-MM-DD') + '"]').append(dayView.render().el);
                        });

                        // Append "empty day" view to days without events inside.
                        var tallestClass = 0;
                        
                        $('[data-day] li').each(function () {
                            console.log($(this).height());
                            if ($(this).height() >= tallestClass) {
                                tallestClass = $(this).height();
                            }
                        });

                        $('[data-day] li').height(tallestClass);

                        $('[data-period="morning"] [data-day]').each(function () {
                            if ($(this).children().length === 0) {
                                var emptyDayView = new EmptyDayView();
                                emptyDayView.listenTo(Backbone.Events, 'classes-list:remove-classes', emptyDayView.remove);
                                $(this).prepend(emptyDayView.render().el);
                            }
                        });

                        window.tagData.search.results = response.classes.length.toString();
                    }


                },
                'error': function (xhr, textStatus) {
                    if (textStatus !== 'abort') {
                        debug('Server Error');
                        ClassesList.loaderAndError.hideLoader();
                        ClassesList.loaderAndError.showError();
                    }
                }
            });
        };

        ClassesList.init = function () {
            Backbone.Events.on('classes-list:remove-filters', function () {
                ClassesList.currentFilters = null;
            });

            // Init Class Search (Bind event for future updates)
            Backbone.Events.on('classes-list:fetch', function (data, date) {
                Backbone.Events.trigger('classes-list:remove-classes');

                if (data) {
                    ClassesList.currentFilters = data;
                }

                if (ClassesList.currentFilters === null || ClassesList.currentFilters.clubs.length === 0) {
                    $('.classes-list .loader-holder').addClass('hidden');
                    $('.results-list').addClass('hidden');
                    var emptyView = new EmptyFilterView();
                    emptyView.listenTo(Backbone.Events, 'classes-list:remove-classes', emptyView.remove);
                    $('.classes-list .error-state').removeClass('hidden').html(emptyView.render().el);
                    window.tagData.search.filterLocationIds = 'na';
                    window.tagData.search.results = '0';
                } else {
                    if (ClassesList.currentFilters.clubs) {
                        window.tagData.search.filterLocationIds = data ? (data.clubs.length === 0 ? 'na' : data.clubs.toString()) : 'na';
                    }
                    if (ClassesList.currentFilters.categories) {
                        window.tagData.search.filterCategoryIds = data ? (data.categories.length === 0 ? 'na' : data.categories.toString()) : 'na';
                    }
                    if (ClassesList.currentFilters.classes) {
                        window.tagData.search.filterClassIds = data ? (data.classes.length === 0 ? 'na' : data.classes.toString()) : 'na';
                    }
                    if (ClassesList.currentFilters.instructors) {
                        window.tagData.search.filterInstructorIds = data ? (data.instructors.length === 0 ? 'na' : data.instructors.toString()) : 'na';
                    }
                    //window._satellite = window._satellite || {};
                    //window._satellite.pageBottom();

                    window.tagData.search.filterClassType = 'na';

                    // If there is a date, re-populate the current week
                    if (date) {
                        ClassesList.currentDate = date;
                        Backbone.Events.trigger('classes-list:generate-week', ClassesList.currentDate);
                    }

                    // Update "current" week
                    var week = moment(ClassesList.currentDate),
                        weekText = week.isSame(moment(), 'week') ? 'This Week' : week.format('D') + '-' + week.add(6, 'day').format('D') + ' ' + week.format('MMM');

                    $('.classes-calendar .current-week').text(weekText);

                    // Reset to first day
                    $('.classes-day-container').find('.classes-day.current').removeClass('current');
                    $('.classes-day-container').find('[data-day="1"]').addClass('current');

                    // Fetch Classes
                    ClassesList.refresh(ClassesList.currentFilters || {}, ClassesList.currentDate || '');
                }


            });

            // Fill in category based from querystring
            if (EQ.Helpers.getQueryStringVariable('categories')) {
                var categories = [];
                $.ajax({
                    type: 'GET',
                    url: APIEndpoint + '/classes/categories',
                    contentType: 'application/json',
                    xhrFields: { 'withCredentials': true },
                    dataType: 'json',
                    success: function (data) {
                        EQ.Helpers.getQueryStringVariable('categories').split(',').forEach(function (c) {
                            var cat = _.find(data, {categoryId: c});
                            if (cat) {
                                categories.push({
                                    displayText: cat.categoryName,
                                    id: c,
                                    score: 1
                                });
                            }
                        });
                        Backbone.Events.trigger('classes-filter:add-filters', {
                            categories: categories
                        });
                    }
                });
            }

            if (EQ.Helpers.getQueryStringVariable('clubs')) {
                // Flatten facilities for easier search
                var flattenedClubs = [];
                _.each(allRegionsData, function (region) {
                    if (region.SubRegions.length === 0) {
                        _.each(region.Facilities, function (facilty) {
                            flattenedClubs.push(facilty);
                        });
                    } else {
                        _.each(region.SubRegions, function (subregion) {
                            _.each(subregion.Facilities, function (facilty) {
                                flattenedClubs.push(facilty);
                            });
                        });
                    }
                });

                // Add to filters
                var clubs = [];

                EQ.Helpers.getQueryStringVariable('clubs').split(',').forEach(function (c) {
                    var club = _.find(flattenedClubs, {'Id': c});
                    if (club) {
                        clubs.push({
                            displayText: club.ClubName,
                            id: c,
                            score: 1
                        });
                    }
                });

                Backbone.Events.trigger('classes-filter:add-filters', {
                    clubs: clubs
                });
            }

            // Instructors
            if (EQ.Helpers.getQueryStringVariable('instructors')) {
                var instructors = [];
                var instructorsIds = [];
                //Check for NaN
                var preIds = EQ.Helpers.getQueryStringVariable('instructors').split(',');

                for (var i = 0; i < preIds.length; i++) {
                    var c = preIds[i];
                    var id = parseInt(c, 0);
                    if (!isNaN(id)) {
                        instructorsIds.push(id);
                    }
                }

                if (instructorsIds.length > 0) {
                    $.ajax({
                        type: 'GET',
                        url: APIEndpoint + '/classes/instructors/' + instructorsIds,
                        contentType: 'application/json',
                        xhrFields: { 'withCredentials': true },
                        dataType: 'json',
                        success: function (data) {
                            console.log(data);
                            instructorsIds.forEach(function (c) {
                                var ins = _.find(data, { 'id': c });
                                console.log(ins, c);
                                if (ins) {
                                    instructors.push({
                                        displayText: ins.displayName,
                                        id: c,
                                        score: 1
                                    });
                                }
                            });
                            Backbone.Events.trigger('classes-filter:add-filters', {
                                instructors: instructors
                            });
                        }
                    });
                }
            }
            
            // Trigger events for initial fetch
            EQ.Helpers.user.getFavorites(function (data) {
                if (data.clubs.length !== 0) {
                    Backbone.Events.trigger('classes-filter:add-filters', {
                        clubs: data.clubs,
                        categories: data.categories

                    });
                } else {
                    Backbone.Events.trigger('classes-list:fetch', null);
                }
            }, function () {
                Backbone.Events.trigger('classes-list:fetch', null);
            });
        };

        ClassesList.init();

        window.track('search', window.tagData.search);
        //window._satellite = window._satellite || {};
        //window._satellite.pageBottom();
    };

}(window.App));