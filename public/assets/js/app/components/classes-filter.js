(function (App) {
    'use strict';

    /* global APIEndpoint, allRegionsData, EQ, Backbone, _ */
    App.Components['classes-filter'] = function ($el, options) {

        // Omniture tagging
        window.tagData = window.tagData || {};
        window.tagData.search = window.tagData.search || {};

        window.tagData.search.filterLocationType = 'na';
        window.tagData.search.filterInstructorType = 'na';
        window.tagData.search.filterCategoryType = 'na';
        window.tagData.search.refinement = 'new';

        var favLocationData = [],
            favCategoryData = [],
            favInstructorData = [];

        // Collections
        var FiltersListCollection = Backbone.Collection.extend({
            initialize: function () {
                this.listenTo(Backbone.Events, 'classes-list:remove-filters', this.reset);
            }
        });

        // Views
        var FilterView = Backbone.View.extend({
            template: _.template($('#addedFilter').html()),
            initialize: function () {
                var that = this;
                this.listenTo(Backbone.Events, 'classes-list:remove-filters', function () {
                    var model = that.model.toJSON();
                    EQ.Helpers.removeQueryStringVariable(model.type, model.id);
                    that.remove();
                });
            },
            events: {
                'click .remove': 'removeFilter'
            },
            removeFilter: function (e) {
                e.preventDefault();

                var model = this.model.toJSON();
                EQ.Helpers.removeQueryStringVariable(model.type, model.id);

                this.remove();
                Backbone.Events.trigger('classes-list:fetch', {
                    clubs: clubsFilterCollection.pluck('id'),
                    instructors: instructorsFilterCollection.pluck('id'),
                    categories: categoriesFilterCollection.pluck('id'),
                    classes: classesFilterCollection.pluck('id')
                });

                if (this.model.attributes.type === 'categories') {
                    window.tagData.search.refinement = '-category';
                    window.tagData.search.filterCategoryType = 'na';
                } else if (this.model.attributes.type === 'clubs') {
                    window.tagData.search.refinement = '-location';
                    window.tagData.search.filterLocationType = 'na';
                } else if (this.model.attributes.type === 'instructors') {
                    window.tagData.search.refinement = '-instructor';
                    window.tagData.search.filterInstructorType = 'na';
                }
            },
            render: function () {
                this.setElement(this.template(this.model.toJSON()));
                return this;
            }
        });

        // Collections
        var clubsFilterCollection = new FiltersListCollection();
        var instructorsFilterCollection = new FiltersListCollection();
        var classesFilterCollection = new FiltersListCollection();
        var categoriesFilterCollection = new FiltersListCollection();

        // Dropdown Views 
        var LocationDropdownView = Backbone.View.extend({
            el: $el.find('[data-autocomplete="clubs"]').find('.autocompleteDropdown'),
            events: {
                'click .region-title': 'expandRegion',
                'click .facility': 'addItem'
            },
            show: function () {
                $('[data-facilities="favorites"]').addClass('open');
                this.$el.show();
            },
            hide: function () {
                // Remove all classes
                this.$el.find('.open').removeClass('open');
                this.$el.find('.active').removeClass('active');
                this.$el.find('.selected').removeClass('selected');

                // Empty selectedItems
                this.selectedItems = [];

                // Hide Dropdown
                this.$el.hide();
            },
            addFilters: function () {
                var that = this;

                if (this.selectedItems.length !== 0) {
                    // Add items to filters
                    var filters = {
                        clubs: that.selectedItems
                    };

                    Backbone.Events.trigger('classes-filter:add-filters', filters);

                    // Omniture Call - 31-05-14
                    window.tagData.search.refinement = '+location';
                    filters.clubs.forEach(function (x) {
                        if ($.inArray(parseInt(x.id, 10), favLocationData) === -1) {
                            window.tagData.search.filterLocationType = 'ind';
                        } else {
                            window.tagData.search.filterLocationType = 'fav';
                        }
                    });
                }
            },
            selectedItems: [],
            addItem: function (e) {
                e.preventDefault();
                var $item = $(e.currentTarget);

                var item = {
                    id: $item.data('id'),
                    displayText: $item.text()
                };

                if (_.some(this.selectedItems, item)) {
                    console.log('rm', this.selectedItems);
                    this.selectedItems = _.reject(this.selectedItems, item);
                    console.log('rm', this.selectedItems);
                } else {
                    this.selectedItems.push(item);
                }

                $item.toggleClass('selected');
            },
            expandRegion: function (e) {
                e.preventDefault();
                var $li = $(e.currentTarget);

                var $region = $('ul[data-region="' + $li.data('region') + '"]'),
                    $facilities = $('ul[data-facilities="' + $li.data('region') + '"]');

                // It's a subregion
                if ($region.length !== 0) {
                    this.$el.find('.open').not($region).not($region.parent()).removeClass('open');
                    $region.toggleClass('open');
                } else { // It's a facilities list
                    this.$el.find('.open').not($facilities).not($facilities.parent()).removeClass('open');
                    $facilities.toggleClass('open');
                }

                if ($li.hasClass('active') === false) {
                    this.$el.animate({
                        scrollTop: this.$el.scrollTop() + $li.position().top
                    });
                }

                this.$el.find('.active').not($li).removeClass('active');
                $li.toggleClass('active');

            },
            render: function () {
                var that = this;
                // Render Favourites
                EQ.Helpers.user.getFavorites(function (data) {
                    if (data.clubs.length !== 0) {
                        that.$el.prepend('<li class="region-title" data-region="favorites"><a href="">Favorite Clubs</a></li>');
                    
                        var $favs = $('<ul data-facilities="favorites"></ul>');
                        
                        _.each(data.clubs, function (facility) {
                            favLocationData.push(facility.id);
                            console.log(favLocationData);
                            $favs.append('<li><a class="facility" href="#" data-id="' + facility.id + '">' + facility.displayText + '</a></li>');
                        });

                        that.$el.find('[data-region="favorites"]').after($favs);

                        // Bind events.
                        that.delegateEvents();
                    }
                });

                // Render AllRegionsData
                _.each(allRegionsData, function (region) {
                    that.$el.append('<li class="region-title" data-region="' + region.ShortName + '"><a href="#">' + region.Name + '</a></li>');

                    if (region.SubRegions.length !== 0) {
                        var $region = $('<ul data-region="' + region.ShortName + '"></ul>');
                        _.each(region.SubRegions, function (subregion) {
                            // Create subregion
                            $region.append('<li class="region-title" data-region="' + subregion.ShortName + '"><a href="#">' + subregion.Name + '</a></li>');
                            // Append facilities to subregion
                            var $facilities = $('<ul data-facilities="' + subregion.ShortName + '""></ul>');

                            _.each(subregion.Facilities, function (facility) {
                                $facilities.append('<li><a class="facility" href="#" data-id="' + facility.Id + '">' + facility.ClubName + '</a></li>');
                            });

                            // Append Subregion to the Region
                            $region.append($facilities);
                        });
                        that.$el.append($region);
                    } else {
                        // Append facilities to subregion
                        var $facilities = $('<ul data-facilities="' + region.ShortName + '""></ul>');

                        _.each(region.Facilities, function (facility) {
                            $facilities.append('<li><a class="facility" href="#" data-id="' + facility.Id + '">' + facility.ClubName + '</a></li>');
                        });
                        that.$el.append($facilities);
                    }
                });
            }
        }),
        CategoriesDropdownView = Backbone.View.extend({
            el: $el.find('[data-autocomplete="classes"]').find('.autocompleteDropdown'),
            events: {
                'click .item-title': 'expandRegion',
                'click .item': 'addItem'
            },
            initialize: function (options) {
                this.type = options.type;
            },
            show: function () {
                $('[data-items="favorites"]').addClass('open');
                $('[data-items="categories"]').addClass('open');
                this.$el.show();
            },
            hide: function () {
                // Remove all classes
                this.$el.find('.open').removeClass('open');
                this.$el.find('.active').removeClass('active');
                this.$el.find('.selected').removeClass('selected');

                // Empty selectedItems
                this.selectedItems = [];

                // Hide Dropdown
                this.$el.hide();
            },
            addFilters: function () {
                var that = this;

                if (this.selectedItems.length !== 0) {
                    // Add items to filters
                    var filters = {
                        categories: that.selectedItems
                    };

                    Backbone.Events.trigger('classes-filter:add-filters', filters);
                    // Omniture Call - 31-05-14

                    window.tagData.search.refinement = '+category';

                    filters.categories.forEach(function (x) {
                        if ($.inArray(parseInt(x.id, 10), favCategoryData) === -1) {
                            window.tagData.search.filterCategoryType = 'ind';
                        } else {
                            window.tagData.search.filterCategoryType = 'fav';
                        }
                    });
                }
            },
            selectedItems: [],
            addItem: function (e) {
                e.preventDefault();
                var $item = $(e.currentTarget);

                var item = {
                    id: $item.data('id'),
                    displayText: $item.text()
                };

                if (_.some(this.selectedItems, item)) {
                    console.log('rm', this.selectedItems);
                    this.selectedItems = _.reject(this.selectedItems, item);
                    console.log('rm', this.selectedItems);
                } else {
                    this.selectedItems.push(item);
                }

                $item.toggleClass('selected');
            },
            expandRegion: function (e) {
                e.preventDefault();
                var $li = $(e.currentTarget);

                var $items = $('ul[data-items="' + $li.data('items') + '"]');

                // It's a subregion
                if ($items.length !== 0) {
                    this.$el.find('.open').not($items).not($items.parent()).removeClass('open');
                    $items.toggleClass('open');
                }

                if ($li.hasClass('active') === false) {
                    this.$el.animate({
                        scrollTop: this.$el.scrollTop() + $li.position().top
                    });
                }

                this.$el.find('.active').not($li).removeClass('active');
                $li.toggleClass('active');

            },
            render: function () {
                var that = this;

                // Render Fav Classes
                EQ.Helpers.user.getFavorites(function (data) {
                    if (data.classes.length !== 0) {
                        that.$el.prepend('<li class="item-title" data-items="favorites"><a href="">Favorite Classes</a></li>');
                    
                        var $favs = $('<ul data-items="favorites"></ul>');
                        
                        _.each(data.classes, function (c) {
                            $favs.append('<li><a class="item" href="#" data-id="' + c.id + '">' + c.displayText + '</a></li>');
                            favCategoryData.push(c.id);
                        });

                        that.$el.find('[data-items="favorites"]').after($favs);

                        that.delegateEvents();
                    }
                });

                if (this.type !== 'bookABike') {
                    $.ajax({
                        type: 'GET',
                        url: APIEndpoint + '/classes/categories',
                        contentType: 'application/json',
                        xhrFields: { 'withCredentials': true },
                        dataType: 'json',
                        success: function (data) {
                            console.log('[categories OK]', data);

                            that.$el.append('<li class="item-title" data-items="categories"><a href="">All Categories</a></li>');

                            var $cats = $('<ul data-items="categories"></ul>');

                            _.each(data, function (cat) {
                                favCategoryData.push(cat.categoryId);
                                $cats.append('<li><a class="item" href="#" data-id="' + cat.categoryId + '">' + cat.categoryName + '</a></li>');
                            });

                            // HARDFIX to remove Personal Training from here.
                            $cats.find('[data-id="103"]').remove();
                            
                            that.$el.append($cats);

                            // Bind events.
                            that.delegateEvents();
                        }
                    });
                }
            }
        }),
        InstructorDropdownView = Backbone.View.extend({
            el: $el.find('[data-autocomplete="instructors"]').find('.autocompleteDropdown'),
            events: {
                'click .item-title': 'expandRegion',
                'click .item': 'addItem'
            },
            show: function () {
                $('[data-items="favorites"]').addClass('open');
                this.$el.show();
            },
            hide: function () {
                // Remove all classes
                this.$el.find('.open').removeClass('open');
                this.$el.find('.active').removeClass('active');
                this.$el.find('.selected').removeClass('selected');

                // Empty selectedItemsº
                this.selectedItems = [];

                // Hide Dropdown
                this.$el.hide();
            },
            addFilters: function () {
                var that = this;

                if (this.selectedItems.length !== 0) {
                    // Add items to filters
                    var filters = {
                        instructors: that.selectedItems
                    };

                    Backbone.Events.trigger('classes-filter:add-filters', filters);
                    // Omniture Call - 31-05-14
                    window.tagData.search.refinement = '+instructor';
                    filters.instructors.forEach(function (x) {
                        if ($.inArray(parseInt(x.id, 10), favInstructorData) === -1) {
                            window.tagData.search.filterInstructorType = 'ind';
                        } else {
                            window.tagData.search.filterInstructorType = 'fav';
                        }
                    });
                }
            },
            selectedItems: [],
            addItem: function (e) {
                e.preventDefault();
                var $item = $(e.currentTarget);

                var item = {
                    id: $item.data('id'),
                    displayText: $item.text()
                };

                if (_.some(this.selectedItems, item)) {
                    console.log('rm', this.selectedItems);
                    this.selectedItems = _.reject(this.selectedItems, item);
                    console.log('rm', this.selectedItems);
                } else {
                    this.selectedItems.push(item);
                }

                $item.toggleClass('selected');
            },
            expandRegion: function (e) {
                e.preventDefault();
                var $li = $(e.currentTarget);

                var $items = $('ul[data-items="' + $li.data('items') + '"]');

                // It's a subregion
                if ($items.length !== 0) {
                    this.$el.find('.open').not($items).not($items.parent()).removeClass('open');
                    $items.toggleClass('open');
                }

                if ($li.hasClass('active') === false) {
                    this.$el.animate({
                        scrollTop: this.$el.scrollTop() + $li.position().top
                    });
                }

                this.$el.find('.active').not($li).removeClass('active');
                $li.toggleClass('active');

            },
            render: function () {
                var that = this;

                // Render Fav Classes
                EQ.Helpers.user.getFavorites(function (data) {
                    if (data.instructors.length !== 0) {
                        that.$el.prepend('<li class="item-title" data-items="favorites"><a href="">Favorite Instructors</a></li>');
                    
                        var $favs = $('<ul data-items="favorites"></ul>');
                        
                        _.each(data.instructors, function (facility) {
                            favInstructorData.push(facility.id);
                            console.log(favInstructorData);
                            $favs.append('<li><a class="item" href="#" data-id="' + facility.id + '">' + facility.displayText + '</a></li>');
                        });

                        that.$el.find('[data-items="favorites"]').after($favs);
                        
                        that.delegateEvents();
                    }
                });
            }
        });

        var locationDropdownView = new LocationDropdownView(),
            categoriesDropdownView = new CategoriesDropdownView({type: options.type}),
            instructorDropdownView = new InstructorDropdownView();

        // Favorites data for the omniture comparision
        //EQ.Helpers.user.getFavorites(function (favoritesData) {
        //    
        //
        //    var favInstructorsId = [];
        //    favoritesData.instructors.forEach(function (x) {
        //        favInstructorsId.push(x.id);
        //    });
        //
        //    var favClassesId = [];
        //    favoritesData.classes.forEach(function (x) {
        //        favClassesId.push(x.id);
        //    });
        //
        //    var favCategoriesId = [];
        //    favoritesData.categories.forEach(function (x) {
        //        favCategoriesId.push(x.id);
        //    });
        //    return favClubsId, favInstructorsId, favClassesId, favCategoriesId;
        //});

        $el.find('[data-autocomplete="clubs"]').each(function () {
            var autocomplete = $(this).data('autocomplete');
            App.loadComponent('autocomplete', $(this), {
                url: APIEndpoint + '/search/autocomplete/' + autocomplete + '/?term=',
                itemSelectedCallback: function (model) {
                    var cid = ('' + model.get('id'));
                    if (clubsFilterCollection.where({ id: (cid) }).length === 0) {
                        model.set('type', autocomplete);

                        clubsFilterCollection.add(model);

                        var FV = FilterView.extend({
                            remove: function () {
                                clubsFilterCollection.remove(this.model);
                                Backbone.View.prototype.remove.call(this);
                            }
                        });

                        var filterView = new FV({
                            model: model
                        });

                        
                        $('.added-filters').prepend(filterView.render().el);

                        Backbone.Events.trigger('classes-list:fetch', {
                            clubs: clubsFilterCollection.pluck('id'),
                            instructors: instructorsFilterCollection.pluck('id'),
                            classes: classesFilterCollection.pluck('id'),
                            categories: categoriesFilterCollection.pluck('id')
                        });

                        window.tagData.search.refinement = '+location';
                        if ($.inArray(parseInt(model.get('id'), 10), favLocationData) === -1) {
                            window.tagData.search.filterLocationType = 'ind';
                        } else {
                            window.tagData.search.filterLocationType = 'fav';
                        }
                    }
                },
                dropdown: locationDropdownView
            });
        });

        $el.find('[data-autocomplete="instructors"]').each(function () {
            var autocomplete = $(this).data('autocomplete');
            App.loadComponent('autocomplete', $(this), {
                url: APIEndpoint + '/search/autocomplete/' + autocomplete + '/?term=',
                itemSelectedCallback: function (model) {
                    var cid = ('' + model.get('id'));
                    console.log(cid, instructorsFilterCollection.where({ id: (cid) }).length, instructorsFilterCollection.pluck('id'));
                    if (instructorsFilterCollection.where({ id: (cid) }).length === 0) {
                        model.set('type', autocomplete);

                        instructorsFilterCollection.add(model);

                        var FV = FilterView.extend({
                            remove: function () {
                                instructorsFilterCollection.remove(this.model);
                                Backbone.View.prototype.remove.call(this);
                            }
                        });

                        var filterView = new FV({
                            model: model
                        });

                        $('.added-filters').prepend(filterView.render().el);

                        Backbone.Events.trigger('classes-list:fetch', {
                            clubs: clubsFilterCollection.pluck('id'),
                            instructors: instructorsFilterCollection.pluck('id'),
                            classes: classesFilterCollection.pluck('id'),
                            categories: categoriesFilterCollection.pluck('id')
                        });

                        window.tagData.search.refinement = '+instructor';
                        if ($.inArray(parseInt(model.get('id'), 10), favInstructorData) === -1) {
                            window.tagData.search.filterInstructorType = 'ind';
                        } else {
                            window.tagData.search.filterInstructorType = 'fav';
                        }
                        
                    }
                },
                dropdown: instructorDropdownView
            });
        });

        $el.find('[data-autocomplete="classes"]').each(function () {
            var autocomplete = $(this).data('autocomplete');
            App.loadComponent('autocomplete', $(this), {
                url: APIEndpoint + '/search/autocomplete/' + autocomplete + '/?term=',
                itemSelectedCallback: function (model) {
                    var cid = ('' + model.get('id'));
                    if (classesFilterCollection.where({ id: (cid) }).length === 0) {
                        model.set('type', autocomplete);

                        classesFilterCollection.add(model);

                        var FV = FilterView.extend({
                            remove: function () {
                                classesFilterCollection.remove(this.model);
                                Backbone.View.prototype.remove.call(this);
                            }
                        });

                        var filterView = new FV({
                            model: model
                        });

                        $('.added-filters').prepend(filterView.render().el);

                        Backbone.Events.trigger('classes-list:fetch', {
                            clubs: clubsFilterCollection.pluck('id'),
                            instructors: instructorsFilterCollection.pluck('id'),
                            classes: classesFilterCollection.pluck('id'),
                            categories: categoriesFilterCollection.pluck('id')
                        });

                        window.tagData.search.refinement = '+category';
                        if ($.inArray(parseInt(model.get('id'), 10), favCategoryData) === -1) {
                            window.tagData.search.filterCategoryType = 'ind';
                        } else {
                            window.tagData.search.filterCategoryType = 'fav';
                        }
                    }
                },
                dropdown: categoriesDropdownView
            });
        });

        // Enable adding filters externally
        Backbone.Events.on('classes-filter:add-filters', function (filters) {
            console.log('filters');

            if (filters && filters.clubs) {
                filters.clubs.forEach(function (club) {
                    var model = new Backbone.Model(club);

                    model.set('type', 'clubs');

                    var cid = ('' + model.get('id'));

                    // Append to queryString
                    EQ.Helpers.setQueryStringVariable('clubs', cid);

                    if (clubsFilterCollection.where({ id: (cid) }).length === 0 && clubsFilterCollection.where({ id: (+cid) }).length === 0) {
                        clubsFilterCollection.add(model);

                        var FV = FilterView.extend({
                            remove: function () {
                                clubsFilterCollection.remove(this.model);
                                Backbone.View.prototype.remove.call(this);
                            }
                        });

                        var filterView = new FV({
                            model: model
                        });

                        $('.added-filters').prepend(filterView.render().el);
                    }
                });
            }
            if (filters && filters.instructors) {
                filters.instructors.forEach(function (club) {
                    var model = new Backbone.Model(club);

                    model.set('type', 'instructors');

                    var cid = ('' + model.get('id'));

                    // Append to queryString
                    EQ.Helpers.setQueryStringVariable('instructors', cid);

                    if (instructorsFilterCollection.where({ id: (cid) }).length === 0 && instructorsFilterCollection.where({ id: (+cid) }).length === 0) {
                        instructorsFilterCollection.add(model);

                        var FV = FilterView.extend({
                            remove: function () {
                                instructorsFilterCollection.remove(this.model);
                                Backbone.View.prototype.remove.call(this);
                            }
                        });

                        var filterView = new FV({
                            model: model
                        });

                        $('.added-filters').prepend(filterView.render().el);
                    }

                });
            }
            if (filters && filters.categories) {
                filters.categories.forEach(function (club) {
                    var model = new Backbone.Model(club);

                    model.set('type', 'categories');

                    var cid = ('' + model.get('id'));

                    // Append to queryString
                    EQ.Helpers.setQueryStringVariable('categories', cid);

                    if (categoriesFilterCollection.where({ id: (cid) }).length === 0 && categoriesFilterCollection.where({ id: (+cid) }).length === 0) {
                        categoriesFilterCollection.add(model);

                        var FV = FilterView.extend({
                            remove: function () {
                                categoriesFilterCollection.remove(this.model);
                                Backbone.View.prototype.remove.call(this);
                            }
                        });

                        var filterView = new FV({
                            model: model
                        });

                        $('.added-filters').prepend(filterView.render().el);
                    }

                });
            }

            if (filters && filters.classes) {
                filters.classes.forEach(function (club) {
                    var model = new Backbone.Model(club);

                    model.set('type', 'classes');

                    var cid = ('' + model.get('id'));

                    // Append to queryString
                    EQ.Helpers.setQueryStringVariable('classes', cid);

                    if (classesFilterCollection.where({ id: (cid) }).length === 0 && classesFilterCollection.where({ id: (+cid) }).length === 0) {
                        classesFilterCollection.add(model);

                        var FV = FilterView.extend({
                            remove: function () {
                                classesFilterCollection.remove(this.model);
                                Backbone.View.prototype.remove.call(this);
                            }
                        });

                        var filterView = new FV({
                            model: model
                        });

                        $('.added-filters').prepend(filterView.render().el);
                    }

                });
            }

            Backbone.Events.trigger('classes-list:fetch', {
                clubs: clubsFilterCollection.pluck('id'),
                instructors: instructorsFilterCollection.pluck('id'),
                categories: categoriesFilterCollection.pluck('id'),
                classes: classesFilterCollection.pluck('id')
            });

        });

        // Toggles the filter editor
        $el.find('.edit-save-filters').on('click', function (e) {
            e.preventDefault();
            $(this).toggleClass('active');
            $('.filter-edit, .selected-filters').slideToggle();
        });

        // Clear filters
        $('.filter-edit .clear-filters').on('click', function (e) {
            e.preventDefault();
            Backbone.Events.trigger('classes-list:remove-filters');
            Backbone.Events.trigger('classes-list:fetch', null);
        });

        window.track('search', window.tagData.search);
        //window._satellite = window._satellite || {};
        //window._satellite.pageBottom();

    };

}(window.App));