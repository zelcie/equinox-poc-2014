(function (App) {
    'use strict';

    /*global EQ, Backbone, _, debug, APIEndpoint, user, moment*/

    App.Components['profile-info'] = function ($el) {
        var UserHeroView = Backbone.View.extend({
            el: $el.find('.header-container'),
            template: _.template($('#userHeroTemplate').html()),
            initialize: function (options) {
                this.options = options || {};
            },
            render: function () {
                if (window.user) {
                    this.$el.html(this.template({
                        firstName: window.user.FirstName,
                        lastName: window.user.LastName,
                        sinceDate: moment(this.options.sinceDate).year()
                    }));
                }
                if (this.options.image) {
                    $('img.profile-image', this.$el).attr('src', this.options.image);
                }
            }
        });

        var SelectView = Backbone.View.extend({
            tagName: 'span',
            className: 'select-wrapper box',
            template: _.template($('#dateDropdownTemplate').html()),
            initialize: function (options) {
                this.options = options || {};
            },
            update: function (newDate) {
                this.$el.html('');

                this.options.data = newDate.data;
                this.options.selected = newDate.selected;

                return this.render();
            },
            render: function () {
                var self = this,
                    select = $(document.createElement('select')).addClass(self.options.name),
                    options = $(document.createElement('span')).addClass('option'),
                    tpl = [],
                    selected;

                $.each(self.options.data, function (index, value) {
                    if (value === self.options.selected) {
                        selected = true;
                    } else {
                        selected = false;
                    }
                    tpl.push(self.template({
                        date: value,
                        active: selected
                    }));
                });

                select.append(tpl);

                options.text(self.options.selected);

                self.$el.prepend([select, options]);

                App.loadComponent(
                    'inline-select',
                    this.$el
                );

                return self;
            }
        });

        var ProfileInfoView = Backbone.View.extend({
            el: $el.find('.personal-info'),
            events: {
                'keyup .weight input': 'numericInput',
                'keyup .height input': 'numericInput',
                'keyup .height input.inches': 'limitInches',
                'change .month-select': 'updateDayList',
                'change .year-select': 'updateDayList',
                'change label.gender-select': 'selectGender',
                'click a.edit-info': 'editMode',
                'click a.save-info': 'saveInfo'
            },
            template: _.template($('#profileInfoTemplate').html()),
            numericInput: function (e) {
                $(e.currentTarget).val($(e.currentTarget).val().replace(/[^0-9\.]/g, ''));
            },
            limitInches: function (e) {
                if (parseInt($(e.currentTarget).val(), 10) > 11) {
                    $(e.currentTarget).val('11');
                }
            },
            selectGender: function (e) {
                var gender = $('input[type="checkbox"]', e.currentTarget).val();
                $('label.gender-select', this.$el).each(function () {
                    if ($(this).hasClass('selected')) {
                        $(this).removeClass('selected');
                        $('input[type="checkbox"]', this).prop('checked', false);
                    }
                });
                $(e.currentTarget).parents('.gender').data('value', gender);
                $(e.currentTarget).toggleClass('selected');
            },
            updateDayList: function () {
                var newDay = parseInt($('.age .day-select', this.$el).val(), 10),
                    month = $('.age .month-select', this.$el).val(),
                    year = $('.age .year-select', this.$el).val(),
                    newDate = moment().month(month).year(year),
                    totalNewDays = newDate.daysInMonth(),
                    days = [];

                if (newDay > totalNewDays) {
                    newDay = 1;
                }

                for (var day = 1; day < totalNewDays + 1; day++) {
                    days.push(day);
                }

                this.daySelectView.update({
                    data: days,
                    selected: newDay
                });

            },
            createCalendar: function () {
                var birthday = this.savedData.birthdate,
                    inputContainer = $('.age .edit .input-container', this.$el),
                    selectCollection = [],
                    today = moment(),
                    years = [],
                    months = moment.monthsShort(),
                    days = [],
                    monthSelectView,
                    yearSelectView;


                // Create Days Select
                for (var day = 1; day < moment(birthday).daysInMonth() + 1; day++) {
                    days.push(day);
                }

                this.daySelectView = new SelectView({
                    data: days,
                    name: 'day-select',
                    selected: moment(birthday).date()
                });

                selectCollection.push(this.daySelectView.render().$el);


                // Create Months Select
                monthSelectView = new SelectView({
                    data: months,
                    name: 'month-select',
                    selected: moment(birthday).format('MMM')
                });

                selectCollection.push(monthSelectView.render().$el);


                // Create Years Select
                for (var year = 1900; year < today.year() + 1; year++) {
                    years.push(year);
                }

                yearSelectView = new SelectView({
                    data: years,
                    name: 'year-select',
                    selected: moment(birthday).year()
                });

                selectCollection.push(yearSelectView.render().$el);

                inputContainer.prepend(selectCollection);

            },
            editMode: function (e) {
                e.preventDefault();
                this.$el.find('.info').hide();
                this.$el.find('.edit').show();

                this.createCalendar();

                if (this.savedData.height.heightUnit === 'in') {
                    this.$el.find('.height .edit .imperial').show();
                } else {
                    this.$el.find('.height .edit .metric').show();
                }

                $(e.currentTarget).hide();
                this.$el.find('.save-info').css('display', 'inline-block');
            },
            saveInfo: function (e) {
                e.preventDefault();
                var self = this,
                    height,
                    unit,
                    birthdate,
                    ENDPOINT = APIEndpoint + '/me/profile';

                if (this.savedData.height.heightUnit === 'in') {
                    var feet,
                        inches;

                    feet = self.$el.find('.height .imperial .feet').val() || self.savedData.height.feet;
                    inches = self.$el.find('.height .imperial .inches').val() || self.savedData.height.inch;
                    height = parseInt(feet * 12, 10) + parseInt(inches, 10);
                    unit = 'Imperial';
                } else {
                    height = parseInt(self.$el.find('.height .metric input').val(), 10);
                    unit = 'Metric';
                }

                birthdate = $('.age .day-select', self.$el).val() + ' ' +
                            $('.age .month-select', self.$el).val() + ' ' +
                            $('.age .year-select', self.$el).val();

                var profile = {
                    age: moment().diff(birthdate, 'years') || self.$el.find('.age').data('age-value'),
                    birthdate: moment(birthdate).toISOString() || self.$el.find('.age').data('birth-value'),
                    gender: self.$el.find('.gender').data('value'),
                    height: height || self.$el.find('.height').data('value'),
                    weight: parseInt(self.$el.find('.weight input').val(), 10) || self.$el.find('.weight').data('value'),
                    unitOfMeasure: unit
                };

                self.loaderAndError.showLoader();

                $.ajax({
                    type: 'PUT',
                    url: ENDPOINT,
                    contentType: 'application/json',
                    xhrFields: { 'withCredentials': true },
                    dataType: 'json',
                    data: JSON.stringify(profile),
                    success: function () {
                        self.loaderAndError.hideLoader();

                        // Update saved profile data to lazily re render.
                        self.savedData.age = (profile.age !== '') ? profile.age : self.savedData.age;
                        self.savedData.birthdate = (profile.birthdate !== '') ? profile.birthdate : self.savedData.birthdate;
                        self.savedData.gender = (profile.gender !== '') ? profile.gender : self.savedData.gender;
                        self.savedData.weight.weight = (profile.weight !== '') ? profile.weight : self.savedData.weight.weight;
                        self.savedData.height.height = (profile.height !== '') ? profile.height : self.savedData.height.height;
                        
                        self.$el.find('.editable .info').show();
                        self.$el.find('.editable .edit').hide();
                        self.$el.find('.edit-info').css('display', 'inline-block');
                        self.$el.find('.save-info').hide();

                        self.render(self.savedData);
                    },
                    error: function (d) {
                        debug('server error', d.responseJSON);
                        self.render(self.savedData);
                        self.loaderAndError.showError();
                    }
                });

            },
            checkUnits: function () {
                var heightData = this.savedData.height,
                    height,
                    inch;

                if (heightData.heightUnit === 'in') {
                    height = heightData.height / 12;
                    height = height.toString().split('.');

                    this.savedData.height.feet = height[0];
                    inch = '0.' + height[1];
                    inch = Math.round(parseFloat(inch, 10) * 12);
                    if (inch === 0) {
                        this.savedData.height.inch = '00';
                    } else {
                        this.savedData.height.inch = inch.toString();
                    }
                }
            },
            render: function (data) {
                var self = this,
                    ENDPOINT = APIEndpoint + '/me',
                    loaderAndError;

                // Page Loader init
                loaderAndError = EQ.Helpers.loaderAndErrorHandler(self.$el.find('.loader'), {
                    errorTitle: 'Error'
                });

                self.$el.find('.half-container').remove();

                loaderAndError.showLoader();

                if (data) {
                    self.checkUnits();
                    loaderAndError.hideLoader();
                    self.$el.prepend(self.template(data));

                    if (!self.savedData.age) {
                        self.$el.find('.age .empty').removeClass('hidden');
                        self.$el.find('.age h3').addClass('hidden');
                    }
                    if (!self.savedData.height.height) {
                        self.$el.find('.height .empty').removeClass('hidden');
                        self.$el.find('.height h3').addClass('hidden');
                    }
                    if (!self.savedData.weight.weight) {
                        self.$el.find('.weight .empty').removeClass('hidden');
                        self.$el.find('.weight h3').addClass('hidden');
                    }

                    if (self.savedData.height.heightUnit === 'in') {
                        self.$el.find('.height .info.imperial').show();
                    } else {
                        self.$el.find('.height .info.metric').show();
                    }
                } else {
                    $.ajax({
                        type: 'GET',
                        url: ENDPOINT,
                        contentType: 'application/json',
                        xhrFields: { 'withCredentials': true },
                        dataType: 'json',
                        success: function (data) {
                            var profileImage,
                                userHeroView;

                            self.savedData = data.profile;

                            debug('DATA LOADED', self.savedData);

                            if (user && user.FacebookId) {
                                profileImage = '//graph.facebook.com/' +
                                                user.FacebookId +
                                                '/picture?width=160&height=160';
                            } else if (self.savedData.profilePictureUrl) {
                                profileImage = self.savedData.profilePictureUrl;
                            }

                            userHeroView = new UserHeroView({image: profileImage, sinceDate: self.savedData.memberSince});

                            userHeroView.render();

                            self.checkUnits();

                            loaderAndError.hideLoader();

                            self.$el.prepend(self.template(self.savedData));


                            //Show empty values and set defaults
                            if (!self.savedData.age) {
                                self.$el.find('.age .empty').removeClass('hidden');
                                self.$el.find('.age h3').addClass('hidden');
                            }
                            if (!self.savedData.birthdate) {
                                self.savedData.birthdate = moment().format();
                            }
                            if (!self.savedData.height.height) {
                                self.savedData.height.height = 0;
                                self.$el.find('.height .empty').removeClass('hidden');
                                self.$el.find('.height h3').addClass('hidden');
                            }
                            if (!self.savedData.gender) {
                                self.savedData.gender = 0;
                                self.$el.find('.gender .empty').removeClass('hidden');
                                self.$el.find('.gender h3').addClass('hidden');
                            }
                            if (!self.savedData.weight.weight) {
                                self.savedData.weight.weight = 0;
                                self.$el.find('.weight .empty').removeClass('hidden');
                                self.$el.find('.weight h3').addClass('hidden');
                                self.$el.find('.weight .edit input').attr('placeholder', '0');
                            }
                            self.$el.find('.edit-info').css('display', 'inline-block');


                            // Save Loader init
                            self.loaderAndError = EQ.Helpers.loaderAndErrorHandler(self.$el.find('.save-info'), {
                                type: 'button',
                                errorTitle: 'Error'
                            });

                            if (self.savedData.height.heightUnit === 'in') {
                                self.$el.find('.height .info.imperial').show();
                            } else {
                                self.$el.find('.height .info.metric').show();
                            }
                        },
                        error: function (d) {
                            debug('server error', d.responseJSON);
                            loaderAndError.showError();
                        }
                    });
                }
            }
        });

        var profileInfoView = new ProfileInfoView();
        profileInfoView.render();

    };

}(window.App));