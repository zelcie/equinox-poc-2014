(function (App) {
    'use strict';

    /* global debug, Backbone, EQ, _ */

    /**
    * Models
    */

    var ItemData = Backbone.Model.extend({
        defaults: {
            id: '',
            category: '',
            displayText: '',
            score: ''
        }
    });

    /**
    * Collections
    */

    var AutoCompleteDataCollection = Backbone.Collection.extend({
        model: ItemData,
        initialize : function (options) {
            this.options = options || {};
        },
        parse: function (response) {
            var finalItems = [],
                that = this;
            _.each(response, function (item) {
                item.type = that.options.type;
                finalItems.push(item);
            });
            return finalItems;
        },
        url: function () {
            var url = this.options.urlEndpoint + this.keyWord;
            return url;
        },
        updateKeyWord: function (keyWord) {
            this.keyWord = keyWord;
        }
    });

    /**
    * Views
    */

    var AutocompleteView = Backbone.View.extend({
        initialize: function (options) {
            this.options = options || {};
        },
        events: {
            'keyup input': 'inputChange',
            'focus input': 'showAutocomplete',
            'click a.close': 'hideAutocomplete',
            'click .autocomplete-toggler': 'toggleAutocomplete'
        },
        inputChange: _.debounce(function (e) {
            e.preventDefault();
            debug('inputChange');

            var that = this,
                $input = $(e.target),
                $autocompleteComponent = this.$el,
                $autocompleteResults = $autocompleteComponent.find('.autocompleteResults'),
                $loaderContainer,
                loaderAndError;

            this.collection.updateKeyWord($input.val());

            // First clean autocomplete
            this.clearAutocomplete();
            // Hide dropwdown
            that.trigger('hideDropdown');

            // Loader needs a container using a mock element
            $autocompleteResults.append('<li><a>.</a></li>');
            $loaderContainer = $autocompleteResults.find('li a');
            loaderAndError = EQ.Helpers.loaderAndErrorHandler($loaderContainer, {
                type: 'button',
                color: 'white'
            });
            loaderAndError.showLoader();

            this.collection.fetch({
                'xhrFields': { 'withCredentials': true },
                'success': function (collection) {
                    debug('search ok', collection);
                    
                    var $autocomplete = this.$el.find('.autocompleteResults');

                    // First clean autocomplete
                    that.clearAutocomplete();

                    // Show
                    $autocomplete.show();

                    // Hide dropwdown
                    that.trigger('hideDropdown');

                    var autocompleteResultsView = new AutocompleteResultsView({
                        collection: collection,
                        el: $autocompleteResults,
                        itemSelectedCallback: function (model) {
                            that.options.itemSelectedCallback(model);
                            that.clearAutocomplete();
                            $autocomplete.hide();
                            $input.val('');
                        }
                    });
                    that.$el.addClass('selected');
                    autocompleteResultsView.render();
                },
                'error': function () {
                    that.clearAutocomplete();
                    debug('Server Error');
                }
            });

        }, 500),
        showAutocomplete: function () {
            this.$el.find('.autocomplete').addClass('focused');
            if ($(window).width() <= 380) {
                $('body').scrollTop((this.$el.find('.autocomplete').offset().top - this.$el.find('.autocomplete').height()) - 20);
            }
            this.trigger('showDropdown');
        },
        hideAutocomplete: function (e) {
            var that = this;
            if (e) {
                e.preventDefault();
            }
            var $el = this.$el;

            $el.find('input').blur();
            $el.find('input').val('');
            $el.find('.autocompleteResults').empty();
            $el.removeClass('selected');
            that.trigger('addDropdownFilters');
            that.trigger('hideDropdown');

            this.$el.find('.autocomplete').removeClass('focused');
        },
        toggleAutocomplete: function () {
            if (this.$el.find('.autocomplete').hasClass('focused')) {
                this.hideAutocomplete();
            } else {
                this.showAutocomplete();
                this.$el.find('input').focus();
            }
        },
        clearAutocomplete: function () {
            var $autocomplete = this.$el.find('.autocompleteResults');
            $autocomplete.html('');
            this.$el.removeClass('selected');
        },
        render: function () {
            var that = this;

            // Bind hideAutocomplete
            $(document).on('mouseup touchend', function (e) {
                if ($(e.target).closest(that.$el).length === 1) {
                    return true;
                } else {
                    that.hideAutocomplete();
                }
            });

            if (this.options.dropdown) {
                this.options.dropdown.render();
                this.options.dropdown.hide();

                this.options.dropdown.$el.on('touchstart touchmove', function () {
                    if ($(window).width() <= 380) {
                        this.$el.find('input').blur();
                        document.activeElement.blur();
                    }
                });

                this.options.dropdown.listenTo(this, 'showDropdown', this.options.dropdown.show);
                this.options.dropdown.listenTo(this, 'hideDropdown', this.options.dropdown.hide);
                this.options.dropdown.listenTo(this, 'addDropdownFilters', this.options.dropdown.addFilters);
            }
            return this;
        }
    });

    var AutocompleteResultsView = Backbone.View.extend({
        initialize: function (options) {
            this.options = options || {};
        },
        render: function () {
            var that = this;
            this.$el.empty();
            this.collection.each(function (item) {
                var autocompleteResultsSingleView = new AutocompleteResultsSingleView({
                    model: item,
                    itemSelectedCallback: that.options.itemSelectedCallback
                });
                this.$el.append(autocompleteResultsSingleView.render().el);
            }, this);
            return this;
        }
    });

    var AutocompleteResultsSingleView = Backbone.View.extend({
        tagName: 'li',
        className: 'single',
        template: _.template($('#autocompleteResultsSingleView').html()),
        events: {
            'click a': 'selectItem'
        },
        initialize: function (options) {
            this.options = options || {};
        },
        selectItem: function (e) {
            e.preventDefault();
            debug('itemSelected');

            // Execute callback
            this.options.itemSelectedCallback(this.model);
        },
        render: function () {
            this.$el.html(this.template(this.model.toJSON()));
            return this;
        }
    });

    var Autocomplete = {};

    Autocomplete.init = function ($el, options) {
        debug('init autocomplete');
        var autocompleteView,
            autoCompleteDataCollection;

        autoCompleteDataCollection = new AutoCompleteDataCollection({
            urlEndpoint: options.url,
            type: options.type
        });

        autocompleteView = new AutocompleteView({
            el: $el,
            collection: autoCompleteDataCollection,
            itemSelectedCallback: options.itemSelectedCallback,
            dropdown: options.dropdown
        });
        autocompleteView.render();
    };

    /**
    * Component Init.
    */

    App.Components.autocomplete = function ($el, options) {
        Autocomplete.init($el, options);
    };

}(window.App));