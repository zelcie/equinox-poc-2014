(function (App) {
    'use strict';

    /* global EQ, APIEndpoint, Backbone, _, debug */

    var ClassDetail = App.Pages.ClassDetail = {};

    /*
     * Models
     */

    var Item = Backbone.Model.extend({
        defaults: {
            id: '',
            type: '',
            selected: false,
            displayName: ''
        }
    });


    /*
     * Models
     */

    var ItemCollection = Backbone.Collection.extend();


    /*
     * Views
     */

    var FavoritesContainerView = Backbone.View.extend({
        el: '.favorites-class-container',
        initialize: function (options) {
            this.options = options || {};
        },
        render: function () {
            var itemCollection = new ItemCollection(),
                favoritesButtonView = new FavoritesButtonView(),
                favoritesListView = new FavoritesListView({
                    collection: itemCollection,
                    favoritesData: this.options.favoritesData,
                    updateCallback: (favoritesButtonView.updateCount).bind(favoritesButtonView)
                });

            favoritesButtonView.render();
            favoritesListView.render();
        }
    });

    var FavoritesButtonView = Backbone.View.extend({
        el: '.favorites-button',
        updateCount: function (number) {
            var content = '';
            if (number !== 0) {
                content = '(' + number + ')';
            }
            this.$el.find('.favorite-count').text(content);
        },
        render: function () {
            var that = this;
            this.$el.on('click', function () {
                that.$el.toggleClass('active');
            });
        }
    });

    var FavoritesListView = Backbone.View.extend({
        el: '.favorites-list',
        initialize: function (options) {
            this.options = options || {};
        },
        updateCount: function () {
            var count = 0;
            this.collection.each(function (item) {
                if (item.get('selected')) {
                    count++;
                }
            });
            this.options.updateCallback(count);
        },
        render: function () {
            var that = this;
            this.$el.find('li').each(function (index, element) {
                var type = $(element).attr('data-type'),
                    id = parseInt($(element).attr('data-id'), 10),
                    data = that.options.favoritesData,
                    item,
                    selected = false,
                    favoritesListSingleView;

                if (_.findIndex(data[type], { id: id }) !== -1) {
                    selected = true;
                }

                item = new Item({
                    displayName: $(element).find('strong').text(),
                    type: type,
                    id: id,
                    selected: selected
                });

                that.collection.add(item);

                favoritesListSingleView = new FavoritesListSingleView({
                    el: element,
                    model: item,
                    updateCallback: (that.updateCount).bind(that)
                });

                favoritesListSingleView.render();
            });
        }
    });

    var FavoritesListSingleView = Backbone.View.extend({
        model: Item,
        initialize: function (options) {
            this.options = options || {};
        },
        toggleSelected: function () {
            this.model.set('selected', !this.model.get('selected'));
            this.options.updateCallback();
            this.$el.find('span').toggleClass('icon-star icon-star-empty');
        },
        updateFavorite: function () {
            var status = this.model.get('selected'),
                ENDPOINT_URL = APIEndpoint + '/me/favorites/' + this.model.get('type'),
                that = this;
            if (!status) {
                this.model.urlRoot = ENDPOINT_URL;
                this.model.save({}, {
                    contentType: 'application/json',
                    xhrFields: { 'withCredentials': true },
                    success: function (model, response) {
                        debug('SAVE MODEL', response);
                        that.toggleSelected();
                    }
                });
            } else {
                ENDPOINT_URL = ENDPOINT_URL + '/remove/' + this.model.get('id');
                $.ajax({
                    type: 'DELETE',
                    url: ENDPOINT_URL,
                    xhrFields: { 'withCredentials': true },
                    contentType: 'application/json'
                }).success(function (response) {
                    debug('DESTROY MODEL', response);
                    that.toggleSelected();
                });
            }
            EQ.Helpers.user.invalidateFavoritesCache();
        },
        render: function () {
            var that = this;
            if (this.model.get('selected')) {
                this.$el.find('span').removeClass('icon-star-empty').addClass('icon-star');
                this.options.updateCallback();
            }
            this.$el.on('click', function () {
                that.updateFavorite();
            });
        }
    });

    var DataBikes = {};
    var DataClass = {};
    var BikeClassDetail = {};
    var RegularClassDetail = {};

    DataBikes.init = function (ENDPOINT, bikeInstanceID) {
        $.ajax({
            type: 'GET',
            url: ENDPOINT,
            contentType: 'application/json',
            xhrFields: { 'withCredentials': true },
            dataType: 'json',
            success: function (data) {
                debug('bikesdata', data);
                if (data.reservation.result !== null) {
                    $('.see-bike, .cancel-class').addClass('active');
                    $('.cancel-class').on('click', function () {
                        DataBikes.cancelBike(bikeInstanceID, $(this));
                    });
                } else {
                    $('.see-bike, .cancel-class').removeClass('active');
                    $('.book-bike, .remove-class').addClass('active');
                }
                if (data.classInstanceDetail.isFinished === true) {
                    $('.see-bike, .book-bike').removeClass('active');
                }
            },
            error: function (d) {
                debug('server error', d.responseJSON);
            }
        });
    };

    DataBikes.cancelBike = function (bikeInstanceID, $el) {
        console.log('CANCEL BIKE');
        var ENDPOINT = APIEndpoint + '/classes/bikes/' + bikeInstanceID + '/cancel',
            loaderAndError;

        loaderAndError = EQ.Helpers.loaderAndErrorHandler($el, {
            type: 'button',
            color: 'black',
            errorTitle: 'Error'
        });
        loaderAndError.showLoader();

        $.ajax({
            type: 'DELETE',
            url: ENDPOINT,
            contentType: 'application/json',
            xhrFields: { 'withCredentials': true },
            dataType: 'json',
            success: function (data) {
                debug('CANCEL BIKE OK', data);
                loaderAndError.hideLoader();

                $('.see-bike, .cancel-class').removeClass('active');
                $('.remove-class, .book-bike').addClass('active');
            },
            error: function (d) {
                debug('server error', d.responseJSON);
                loaderAndError.showError();
            }
        });
    };

    DataClass.init = function (CLASSENDPOINT) {
        $.ajax({
            type: 'GET',
            url: CLASSENDPOINT,
            contentType: 'application/json',
            xhrFields: { 'withCredentials': true },
            dataType: 'json',
            success: function (data) {
                debug('classdata', data);
                window.tagData = window.tagData || {};

                var timeOffset = window.moment(data.startLocal).diff(data.facilityCurrentDateTime, 'hours'),
                    timeOffsetMinutes = window.moment(data.startLocal).diff(data.facilityCurrentDateTime, 'minutes');

                if (timeOffset > 0) {
                    timeOffset = Math.floor(timeOffset);
                } else {
                    timeOffset = Math.ceil(timeOffset);
                }

                if (timeOffset === 0) {
                    if (timeOffsetMinutes > 0) {
                        timeOffset = 1;
                    } else {
                        timeOffset = -1;
                    }
                }
                window.tagData.classInstance = {
                    'classId': data.classId.toString(),
                    'facilityId': data.facility.facilityId,
                    'classInstanceId': data.classInstanceId.toString(),
                    'categoryId': data.primaryCategory.categoryId.toString(),
                    'timeOffset': timeOffset.toString()
                };

                // Make data available on other methods
                DataClass.jsonData = data;

                if (data.isOnCalendar === false) {
                    $('.add-class').addClass('active');
                } else {
                    $('.export-to-calendar').addClass('active');

                    if (data.isCyclingClass === true) {
                        var ENDPOINT = APIEndpoint + '/classes/bikes/' + BikeClassDetail.classInstance;

                        DataBikes.init(ENDPOINT, BikeClassDetail.classInstance);
                    } else {
                        $('.remove-class').addClass('active');
                    }
                }

                $('.add-class').on('click', DataClass.addClass);
                $('.remove-class').on('click', DataClass.removeClass);
            },
            error: function (d) {
                debug('server error', d.responseJSON);
            }
        });
    };

    DataClass.addClass = function (e) {
        e.preventDefault();
        var ENDPOINT = APIEndpoint + '/me/calendar/' + BikeClassDetail.classInstance + '?isRecurring=false',
            loaderAndError;

        loaderAndError = EQ.Helpers.loaderAndErrorHandler($(this), {
            type: 'button',
            color: 'white',
            errorTitle: 'Error'
        });
        loaderAndError.showLoader();

        $.ajax({
            type: 'POST',
            url: ENDPOINT,
            contentType: 'application/json',
            xhrFields: { 'withCredentials': true },
            dataType: 'json',
            success: function (data) {
                debug('[ADDCLASS OK]', data);

                loaderAndError.hideLoader();

                // Update userEventId to use it later on removeClass()
                DataClass.jsonData.userEventId = data.result.id;

                if (DataClass.jsonData.isCyclingClass === true) {
                    $('.bike-overlay.add, .remove-class, .book-bike, .export-to-calendar').addClass('active');
                    $('.add-class').removeClass('active');
                    setTimeout(function () {
                        $('.bike-overlay.add').removeClass('active');
                    }, 5000);
                } else {
                    $('.overlay-box.add, .export-to-calendar, .remove-class').addClass('active');
                    $('.add-class').removeClass('active');
                    setTimeout(function () {
                        $('.overlay-box.add').removeClass('active');
                    }, 5000);
                }
            },
            error: function (d) {
                debug('server error', d.responseJSON);
                loaderAndError.showError();
            }
        });
    };

    DataClass.removeClass = function (e) {
        e.preventDefault();
        var ENDPOINT = APIEndpoint + '/me/calendar/cancel/' + DataClass.jsonData.userEventId + '?removeRecurring=false',
            loaderAndError;

        loaderAndError = EQ.Helpers.loaderAndErrorHandler($(this), {
            type: 'button',
            color: 'black',
            errorTitle: 'Error'
        });
        loaderAndError.showLoader();

        $.ajax({
            type: 'DELETE',
            url: ENDPOINT,
            contentType: 'application/json',
            xhrFields: { 'withCredentials': true },
            dataType: 'json',
            success: function (data) {
                debug('[REMOVECLASS OK]', data);
                loaderAndError.hideLoader();

                $('.overlay-box.remove, .add-class').addClass('active');
                $('.remove-class, .see-bike, .book-bike, .export-to-calendar').removeClass('active');
                setTimeout(function () {
                    $('.overlay-box.remove').removeClass('active');
                }, 5000);
            },
            error: function (d) {
                debug('server error', d.responseJSON);
                loaderAndError.showError();
            }
        });
    };

    ClassDetail.renderButtons = function () {
        BikeClassDetail.classInstance = $('nav.buttons').data('id');
        RegularClassDetail.classInstance = BikeClassDetail.classInstance;

        var CLASSENDPOINT = APIEndpoint + '/classes/' + RegularClassDetail.classInstance;

        DataClass.init(CLASSENDPOINT);
    };

    ClassDetail.updateOmniture = function () {

        if ($(document).find($('#classDetailsInfo'))) {
            window.tagData = window.tagData || {};
            var classDetails = JSON.parse($('#classDetailsInfo').attr('data-component-options')),
                timeOffset = window.moment(classDetails.startLocal).diff(classDetails.facilityCurrenttime, 'hours'),
                timeOffsetMinutes = window.moment(classDetails.startLocal).diff(classDetails.facilityCurrenttime, 'minutes');

            if (timeOffset) {

                if (timeOffset > 0) {
                    timeOffset = Math.floor(timeOffset);
                } else {
                    timeOffset = Math.ceil(timeOffset);
                }

                if (timeOffset === 0) {
                    if (timeOffsetMinutes > 0) {
                        timeOffset = 1;
                    } else {
                        timeOffset = -1;
                    }
                }
            }
            window.tagData.classInstance = {
                'classId': classDetails.classID,
                'facilityId': classDetails.facilityID,
                'classInstanceId': classDetails.classInstanceID,
                'categoryId': classDetails.categoryID,
                'timeOffset': timeOffset.toString()
            };
        }
    };

    ClassDetail.init = function (id) {

        console.log(id);

        EQ.Helpers.user.getFavorites(function (favoritesData) {
            var favoritesContainerView = new FavoritesContainerView({ favoritesData: favoritesData });
            favoritesContainerView.render();
        });

        ClassDetail.renderButtons();

        ClassDetail.updateOmniture();
        
    };

}(window.App));