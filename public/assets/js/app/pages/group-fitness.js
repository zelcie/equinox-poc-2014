(function (global, App) {
    'use strict';

    App.Pages.GroupFitness = {};
    var GroupFitness = App.Pages.GroupFitness;

    GroupFitness.init = function () {
        $(window).on('resize scroll', GroupFitness.expandQuilt);
        GroupFitness.expandQuilt();
    };

    GroupFitness.expandImage = function (e) {
        e.preventDefault();

        $('.grid-active [class*="image-"]').remove();

        $(this).find('.quilt-overlay').clone().fadeIn().appendTo('.grid-active').addClass('active');
        $('.grid-active').fadeIn().addClass('active');
        $('.groupfitness').addClass('overlay');

        setTimeout(function () {
            $('.grid-active .quilt-overlay').addClass('overlay');
            $('body').css({'overflow': 'hidden'});
        }, 100);
        
        $('.grid-active .close').on('click', function (e) {
            e.preventDefault();
            
            $('.grid-active .quilt-overlay').removeClass('overlay');
            $('body').removeAttr('style');
            $('.groupfitness').removeClass('overlay');
            $('.grid-active').fadeOut().removeClass('active');
            
            setTimeout(function () {
                $('.grid-active .quilt-overlay.active').remove();
            }, 700);

        });
    };

    GroupFitness.expandQuilt = function () {
        var images = $('[class*="image-"]');
        images.off('click').on('click', GroupFitness.expandImage);
    };

} (window, window.App));