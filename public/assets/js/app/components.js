﻿/* data-component library - all subscritps are loaded from app/components */
(function (App) {
    'use strict';

    /* global debug, require, Backbone, _ */

    var createComponent = function (component, $el, options) {
        return App.Components[component]($el, options);
    };

    App.renderComponents = function (selector) {
        var $selector = $(selector),
            components = [],
            requireComponents = [];

        //For each component, add it to the load list.
        $selector.find('[data-component]').each(function (i, el) {
            var $el = $(el),
                component = $el.data('component'),
                options = $el.data('component-options');

            try {
                debug('[DataComponent] Loading: ' + component + ' component.');

                var C = {
                    filename: '/assets/js/app/components/min/' + component + '.js',
                    $el: $el,
                    component: component,
                    options: options
                };

                components.push(C);
                requireComponents.push(C.filename);

            } catch (e) {
                console.error('data-component not found: ' + component, e.stack);
            }
        });

        try {
            // Load them with requirejs and after all the components are loaded, run them.
            require(requireComponents, function () {
                // Iterate over the components and initialize them.
                _.forEach(components, function (c) {
                    createComponent(c.component, c.$el, c.options || {});
                });
            });
        } catch (e) {
            console.error('data-component not found', e.stack);
        }


    };

    /*
    * Load a component and  bind it to an $el. Useful for loading components without DOM data-elements
     */
    App.loadComponent = function (component, $el, options) {
        debug('[DataComponent] Loading: ' + component + ' component.');
        
        require(['/assets/js/app/components/min/' + component + '.js'], function () {
            createComponent(component, $el, options || {});
        });
    };

    // Render Components.
    App.renderComponents('body');

    // Add renderComponents to Backbone.View (Call this after View.render to process data-components)
    _.extend(Backbone.View.prototype, Backbone.Events, {
        renderComponents: function ($el) {
            App.renderComponents($el);
        }
    });

}(window.App));
