define(function (require, exports, module) {
    'use strict';

    var App = require('app');
    var transaction = App.store.transaction; // Alias the model for easier identification.
    var Repurchase = module.exports;

    //var OptionModel = Backbone.Model.extend();
    //var OptionViews = Backbone.View.extend();
    //
    //var sessionsOptionModel = new OptionModel({
    //    name: 'sessions'
    //});
    ////
    //var sessionsOptionView = new OptionViews({
    //    model: sessionsOptionModel
    //});


    //
    //
    //    var Options = {};
    //
    //    Options.View = Backbone.View.extend({
    //        manage: true,
    //        template: 'step3/option',
    //        events: {
    //            'change select': function () {
    //                this.selected();
    //                this.triggerEvents();
    //            }
    //        },
    //        selected: function () {
    //            var str = this.$('option:selected').text();
    //            var key = parseInt(str.replace(/^\D+/g, ''), 10);
    //            this.$('.option').text(str);
    //
    //            key = isNaN(key) ? 0 : key;
    //
    //            that.model.set({
    //                autoRenewPackSize: key
    //            });
    //
    //            if(_.isEqual(key, 0)) {
    //                transaction.set({isAutoRenewOn: false});
    //            }
    //            else {
    //                transaction.set({isAutoRenewOn: true});
    //            }
    //        }
    //    });
    //
    //    Options.Model = Backbone.Model.extend({
    //        defaults: {
    //            options: [],
    //            disable: true,
    //            name: null,
    //            selected: 'Select'
    //        },
    //
    //        url: App.envConfig.packsizeUrl + urlFragment
    //    });
    //
    //    sessionsOptionModel = new Options.Model({
    //        name: 'sessions'
    //    });
    //
    //    sessionsOptionModel.fetch().then(function (data) {
    //        sessionsOptionModel.set({
    //            options: data, //that.createObject(selectedSessions),
    //            disable: false
    //        });
    //
    //        var selected = transaction.get('isAutoRenewOn') ? transaction.get('autoRenewPackSize') + ' Sessions' : 'No Repurchase';
    //
    //        sessionsOptionModel.set({
    //            selected: selected
    //        });
    //
    //        sessionsOptionView = new Options.View({
    //            model: sessionsOptionModel,
    //            triggerEvents: function () {
    //                App.trigger('update:selected:packageSize', this.$('option:selected').val());
    //            }
    //        });
    //
    //        that.setView('#sessionsOption', sessionsOptionView).render();
    //    });
    //},
    //
    //sessionsOptions: {
    //    '0': 'No Repurchase',
    //    '1': '1 Session',
    //    '8': '8 Sessions',
    //    '12': '12 Sessions',
    //    '24': '24 Sessions'
    //},
    //
    //sessionsByDuration: {
    //    '60': ['0', '1', '12', '24'],
    //    '30': ['0', '1', '8']
    //},


    // Models
    Repurchase.Models = {};
    // Collections
    Repurchase.Collections = {};
    // Views
    Repurchase.Views = {};


    //Repurchase.Models.Option = Backbone.View.extend({
    //    defaults: {
    //        label: 'Select',
    //        val: null
    //    }
    //});

    Repurchase.Views.Option = Backbone.View.extend({
        manage: true,
        el: false,
        template: 'step3/option',

        events: {
            'change select': function () {
                this.selected();
                this.triggerEvents();
            }
        },

        serialize: function () {
            return this.model.toJSON();
        },

        selected: function () {
            //var str = this.$('option:selected').text();
            //var key = parseInt(str.replace(/^\D+/g, ''), 10);
            //this.$('.option').text(str);
            //
            //key = isNaN(key) ? 0 : key;
            //
            //that.model.set({
            //    autoRenewPackSize: key
            //});
            //
            //if (_.isEqual(key, 0)) {
            //    transaction.set({isAutoRenewOn: false});
            //}
            //else {
            //    transaction.set({isAutoRenewOn: true});
            //}
        }
    });


    Repurchase.Views.Base = Backbone.View.extend({
        manage: true,
        template: 'repurchase',

        events: {
            'click .toggle-sessions-dropdown': 'toggleSessionsDropDown',
            'change .repurchase-dropdown select': 'onSessionsDropdownChange'
        },

        onSessionsDropdownChange: function () {
            console.log('dropdown change');
            this.$('.option').text(this.$('option:selected').text());
        },

        toggleSessionsDropDown: function (e) {
            e.preventDefault();
            console.log('dropdown update');

            var $repurchaseDropdown = $('.repurchase-dropdown');

            if ($repurchaseDropdown.hasClass('expanded')) {
                $repurchaseDropdown.slideUp().removeClass('expanded');
            } else {
                $repurchaseDropdown.slideDown().addClass('expanded');
            }
        },

        //getSelectedSessions: function () {
        //    var selectedPackage = transaction.get('selectedPackage');
        //    var packageId = selectedPackage.packageId;
        //    var packages = transaction.get('packages');
        //
        //    console.log('drop down', selectedPackage);
        //    return packages._byId[packageId].attributes.quantity;
        //},
        //
        //updateSessions: function () {
        //   // var that = this;
        //    var selectedPackage = transaction.get('selectedPackage');
        //    var duration = (selectedPackage.duration).replace(/[A-Za-z$-]/g, '').replace(' ', '');
        //   // var urlFragment = '/' + transaction.get('selectedDuration') + '/' + transaction.get('selectedTier');
        //    transaction.set({autoRenewDuration: duration});
        //
        //    App.on('update:selected:packageSize', function (data) {
        //        transaction.set({packageSize: data});
        //    });
        //},

        beforeRender: function () {

            this.collection.each(function (model) {
                this.insertView('.select-repurchase', new Repurchase.Views.Option({
                    model: model
                }));
            }, this);


            //var updateSession = false;

            //transaction.on('change:autoRenewPackSize', function () {
            //    // if (!updateSession) {
            //    this.updateSessions();
            //    //  updateSession = true;
            //    //  }
            //}, this);
            //
            //if (transaction.get('hasCardOnFile') || transaction.get('useCardOnFile')) {
            //    transaction.set('autoRenewPackSize', this.getSelectedSessions());
            //}
        },


        updateLabel: function () {
            var $repurchaseDropdown = $('#select-repurchase');
            var label = $repurchaseDropdown.find('option:selected').text();
            console.log('label', label);
            $repurchaseDropdown.next('.option').text(label);
        },

        afterRender: function () {
            this.updateLabel();
        },

        serialize: function () {
            //return _.pick(transaction.toJSON(), 'useCardOnFile', 'cardType', 'cardLastFourDigits', 'cardLastFourDigitsNew');
            return transaction.toJSON();
        }
    });
});


