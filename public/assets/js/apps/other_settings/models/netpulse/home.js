define(function (require, exports, module) {
    'use strict';

    var Backbone = require('backbone');
    var Core = require('core/app');

    /**
     * Other Settings HP Model
     *
     * Model for the othersettings homepage
     *
     * @augments Backbone.Model
     * @name AccountHome
     * @class AccountHomeModel
     * @return model
     */
    var Model = Backbone.Model.extend({

        url: Core.utils.getApi('/othersettings/nplinkstatus'),

        defaults: {
            title: 'Net Pulse',
            copy: '<p>To create a complete fitness profile you can link your account to many fitness and health tracking apps. linking your accounts takes about a minute. just click on the “link” button of the app you would like to link to and follow the prompts to authenticate.</p> <p>once you have linked your accounts, all tracking will be synced to your web account the next time you log in.</p>'
        }

    });

    module.exports = Model;
});
