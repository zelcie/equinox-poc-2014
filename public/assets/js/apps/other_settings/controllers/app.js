define(function (require, exports, module) {
    'use strict';

    var Marionette  = require('marionette');

    /**
     * App Controller
     *
     * This is the base controller for the account app. In here, we simply manage
     * the firing of the appropriate sub-controller logic for each page. Note how
     * we do not require all Views, Models, Layouts and Forms on top. Instead, we
     * we load them only when needed via a require() call inside each method. This
     * will ensure the app does not load too much into memory.
     *
     * @augments Backbone.Model
     * @name AccountApp
     * @class AppController
     * @return model
     */
    var Controller = Marionette.Controller.extend({

        /**
         * Account Info
         *
         * @name AppController#account
         * @function
         * @public
         */
        otherSettings: function(id) {
            require(['other_settings/controllers/home'], function (Controller) {
                var controller = new Controller();

                controller.init(id);
            });
        },

        netpulse: function() {
            require(['other_settings/controllers/netpulse/home'], function (Controller) {
                var controller = new Controller();

                controller.init();
            });
        },

        netpulseSuccess: function() {
            require(['other_settings/controllers/netpulse/home'], function (Controller) {
                var controller = new Controller();

                controller.init({success:true});
            });
        },

        netpulseLogin: function() {
            require(['other_settings/controllers/netpulse/login'], function (Controller) {
                var controller = new Controller();

                controller.init();
            });
        },

        netpulseRegister: function() {
            require(['other_settings/controllers/netpulse/register'], function (Controller) {
                var controller = new Controller();

                controller.init();
            });
        }

    });

    module.exports = Controller;
});