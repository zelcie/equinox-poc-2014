/* global define, Backbone, Marionette, App, consts */

define(function (require, exports, module) {
    'use strict';

    var Core            = require('core/app');
    var Marionette      = require('marionette');
    var Model           = require('other_settings/models/netpulse/login');
    var View            = require('other_settings/views/netpulse/login');

    /**
     * Other Settings Netpulse Signin Controller
     *
     * Controller used to manage everything for the /othersettings homepage
     *
     * @augments Backbone.Model
     * @name OtherSettingsHome
     * @class OtherSettingsHomeController
     * @return model
     */
    var Controller = Marionette.Controller.extend({

        /**
         * Init
         *
         * @name AccountHomeController#init
         * @function
         * @public
         */
        init: function() {
            var model, view;

            model = new Model();
            view  = new View({ model: model });

            Core.addRegions({ mainRegion: Core.el });
            Core.mainRegion.show(view);

        }

    });

    module.exports = Controller;
});