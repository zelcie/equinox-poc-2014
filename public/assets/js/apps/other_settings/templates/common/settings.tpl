<div class="settings-table">
    <div class="row">
        <div class="col-1-2">
            <h3>Email Subscriptions</h3>
            <p>To change email address, go to <a href="/account#username/edit">Account Settings</a></p>
        </div>
        <div class="col-1-2">
            <p><a href="#" class="todo">Edit</a></p>
        </div>
    </div>

    <div class="row">
        <div class="col-1-2">
            <h3>Facebook Share</h3>
            <p>Your Facebook friends can see the classes on your calendar. It only affects on the Equinox website and mobile app, and does not post it to Facebook.</p>
        </div>
        <div class="col-1-2">
            <p><%= facebookSwitch() %></p>
            <p><a href="#" class="todo">ON/OFF</a></p>
        </div>
    </div>
</div>