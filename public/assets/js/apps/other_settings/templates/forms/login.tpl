<div class="form-error error"></div>

<fieldset class="narrow-fieldset">
    <fieldset class="fieldset-row">
        <div data-fields="clientLoginId"></div>
        <div><label>Enter 10-14 digit number</label></div>
    </fieldset>

    <fieldset class="fieldset-row">
        <div data-fields="passcode"></div>
        <div><label>4 digit numeric</label></div>
    </fieldset>
</fieldset>