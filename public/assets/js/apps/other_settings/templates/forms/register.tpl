<div class="form-error error"></div>

<fieldset class="narrow-fieldset">
    <fieldset class="fieldset-row">
        <div data-fields="clientLoginId"></div>
        <div><label>Enter 10-14 digit number</label></div>
    </fieldset>

    <fieldset class="fieldset-row">
        <div data-fields="passcode"></div>
        <div><label>4 digit numeric</label></div>
    </fieldset>

    <fieldset class="fieldset-row">
        <div data-fields="passcodeConfirm"></div>
        <div><label>Confirm passcode</label></div>
    </fieldset>
</fieldset>

<hr />

<fieldset class="narrow-fieldset">
    <h3>Gender</h3>

    <fieldset class="fieldset-row gender-field">
        <div data-fields="gender"></div>
    </fieldset>

    <fieldset class="fieldset-row">
        <div data-fields="birthDay"></div>
    </fieldset>

    <fieldset class="fieldset-row weight-field">
        <fieldset>
            <div data-fields="weight"></div>
        </fieldset>
        <fieldset>
            <label class="weight-unit">LBS</label>
        </fieldset>
    </fieldset>

    <h3>Unit of Measure</h3>

    <fieldset class="fieldset-row measure-unit-field">
        <div data-fields="measurementUnit"></div>
    </fieldset>
</fieldset>

<hr />

<fieldset class="narrow-fieldset">
    <fieldset class="fieldset-row agreement-field">
        <div data-fields="acceptedTerms"></div>
    </fieldset>
</fieldset>

