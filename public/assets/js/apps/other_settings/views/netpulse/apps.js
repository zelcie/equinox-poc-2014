/* global, define, _ */
define(function (require, exports, module) {
    'use strict';

    var Marionette = require('marionette');
    var Template1  = require('text!other_settings/templates/netpulse/home.tpl');
    var Template2  = require('text!other_settings/templates/netpulse/apps.tpl');

    var ItemView = Backbone.Marionette.ItemView.extend({
        template: _.template(Template2),
        tagName: 'li'
    });

    /**
     * Partial View Sample
     *
     * Regular Marionette ItemView Partial
     *
     * @name Partial1
     * @class PartialView1
     * @return view
     */
    var View = Marionette.CompositeView.extend({
        template: _.template(Template1),
        itemView: ItemView,
        itemViewContainer: 'ul'

    });

    module.exports = View;
});