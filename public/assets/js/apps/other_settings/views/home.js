define(function (require, exports, module) {
    'use strict';

    var Marionette   = require('marionette');
    var BossView     = require('bossview');
    var HeadingView  = require('core/views/common/heading');
    var SettingsView = require('other_settings/views/common/settings');
    var ServicesView = require('other_settings/views/common/services');
    //var PersonalView   = require('views/common/personal');

    /**
     * Account Homepage View
     *
     * This is the master view for the account homepage
     *
     * @name Page
     * @class PageView
     * @return view
     */
    var View = Marionette.BossView.extend({

        subViews: {
            heading: HeadingView,
            settings: SettingsView,
            services: ServicesView
             /*,
            membership: function() {
                return new MembershipView({
                    model: this.model.get('membership')
                });
            },
            balance: function() {
                return new BalanceView({
                    model: this.model.get('billing')
                });
            },
            personal: function() {
                return new PersonalView({
                    model: this.model.get('membership')
                });
            } */
        }

    });

    module.exports = View;
});