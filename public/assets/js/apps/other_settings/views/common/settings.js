/* global, define, _ */

define(function (require, exports, module) {
    'use strict';

    var Marionette = require('marionette');
    //var Backbone = require('backbone');
    var Template   = require('text!other_settings/templates/common/settings.tpl');
    //var SwitchView = require('other_settings/views/common/switch.js');

    /**
     * Username Confirmation View
     *
     * This is the view, template helpers and bindings for the username confirmation page
     * which is shown after a user submits the a username/edit form
     *
     * @name UsernameConfirmation
     * @class UsernameConfirmationView
     * @return view
     */
    var View = Marionette.ItemView.extend({
        template : _.template(Template),

        templateHelpers: {
            facebookSwitch: function() {
                var SwitchView = Backbone.Form.extend({
                    //template: _.template('<div data-field="facebook"></div>'),

                    schema: {
                        isBillingOptOut : {
                            type: 'Switch',
                            options: ['On', 'Off']
                        }
                    }
                });

                var MyModel = Backbone.Model.extend();

                var switchView = new SwitchView({model: new MyModel() });

                switchView.render();

                return switchView.$el.html();
            }
        }
    });

    module.exports = View;
});