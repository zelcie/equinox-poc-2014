/* global, define, _ */

define(function (require, exports, module) {
    'use strict';

    var Marionette = require('marionette');
    var $sitecore  = $('.module-3col-points').clone();
    var Template   = $sitecore[0].outerHTML + require('text!other_settings/templates/services.tpl');
    //var Template   = $sitecore[0].outerHTML;

    console.log(Template);

    $('.module-3col-points').remove(); // destroy original

    /**
     * Services View
     *
     * This is the view, template helpers and bindings for the services component
     * seen on the Other Settings landing page
     *
     * @name Services
     * @class ServicesView
     * @return view
     */
    var View = Marionette.ItemView.extend({
        //template : _.template(Template)
        el: '<div class="three-points-table"></div>',
        template: _.template(Template)
    });

    module.exports = View;
});