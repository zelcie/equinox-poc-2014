/* global Backbone, define, App, _ */

define(function (require, exports, module) {
    'use strict';

    var Form;
    var Marionette = require('marionette');
    var Model      = require('core/models/form_utils');
    var Addons     = require('core/helpers/backbone/forms/extensions');
    var Template   = require('text!core/templates/forms/password.tpl');

    Form = Backbone.Form.extend({

        template: _.template(Template),

        schema: {

            currentPassword: {
                title: 'Type in your current Password',
                type: 'Password',
                validators: ['required']
            },

            newPassword: {
                title: 'Type in new Password',
                type: 'Password',
                validators: [
                    'required',
                    { type: 'match', field: 'passwordConfirm', message: 'Passwords must match!' }
                ]
            },

            passwordConfirm: {
                title: 'Type Again',
                type: 'Password',
                validators: ['required']
            }

        },
        idPrefix: 'password-'
    });

    module.exports = Form;
});