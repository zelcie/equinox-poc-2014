<div class="agreement">

    <div class="checkbox">
        <label>
            <span class="icon-check checkbox-replacement"></span>
        </label>
        <span class="label"><%= title %></span>
    </div>

    <div class="collapsible">
        <p>Additional goods and services - in addition to monthly payments for membership fees and dues (if applicable), i, <span class="name-on-card"></span>, authorize equinox to keep my credit card or bank account information identified below on file and to post charges to such account for the cost of additional goods and services which i, buyer, purchase at an equinox club. These other charges may include fees for personal training, spa and/or fitness training sessions, retail apparel and other similar items, and food and beverage items. Buyer recognizes and agrees that equinox will maintain this credit card or bank account information in its files and acknowledges equinox’s right to charge the account identified at any time and from time to time during the term of my membership.<br />
        Specific acknowledgment if buyer is not the member - i, marvin martian, specifically understand and agree that if buyer is not the member, my credit card or bank account may be charged for items purchased by the member and by giving my bank account or credit card information below, i consent to the cost of such items being posted and charged by equinox to my account.</p>

        <p>I may withdraw my authorization for use of my credit card or bank account for additional goods and services upon written notice to equinox.<br /> 
        I understand that this document serves as an amendment to the electronic funds transfer portion of the original agreement and that all terms and conditions of the original membership agreement apply.</p>
    </div>

</div>