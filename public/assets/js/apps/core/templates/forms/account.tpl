<div class="form-error error"></div>

<fieldset class="fieldset-row">
    <div data-fields="address1"></div>
</fieldset>

<fieldset class="fieldset-row">
    <div data-fields="address2"></div>
</fieldset>

<fieldset class="fieldset-row location-fields">
    <div data-fields="city,state,zipCode,country"></div>
</fieldset>

<fieldset class="fieldset-row phone-fields">
    <label>Phone: the top one will be displayed as a primary.</label>
    <div data-fields="phone,phoneTypes,phoneCarrier,workPhone"></div>
</fieldset>