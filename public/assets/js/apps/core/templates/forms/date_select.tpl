<fieldset class="date-fields">
    <div class="fancy-select date" style="display: none;">
        <span class="dropdown block">
            <span class="option"></span>
            <select data-type="date"><%= dates %></select>
        </span>
    </div>

    <div class="fancy-select month">
        <span class="dropdown block">
            <span class="option"></span>
            <select data-type="month"><%= months %></select>
        </span>
    </div>

    <div class="fancy-select year">
        <span class="dropdown block">
            <span class="option"></span>
            <select data-type="year"><%= years %></select>
        </span>
    </div>

</fieldset>

        <!--//
                <div>

    <span class="fancy-select select-wrapper date" style="display: none;">
        <span class="option"></span>
        <select data-type="date"><%= dates %></select>
    </span>

    <span class="fancy-select select-wrapper month">
        <span class="option"></span>
        <select data-type="month"><%= months %></select>
    </span>

    <span class="fancy-select select-wrapper year">
        <span class="option"></span>
        <select data-type="year"><%= years %></select>
    </span>

</div>

                //-->