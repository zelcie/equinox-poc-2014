<div class="fancy-switch">
    <label class="switch switch-t-text">
        <input type="checkbox" class="switch-input" checked>
        <span class="switch-label" data-on="On" data-off="Off"></span>
        <span class="switch-handle"></span>
    </label>
</div>

<!--//
<label class="switch">
    <input type="checkbox" class="switch-input" checked>
    <span class="switch-label" data-on="On" data-off="Off"></span>
    <span class="switch-handle"></span>
</label>
//--->