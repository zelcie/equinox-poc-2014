<div data-type="phone"></div>

<div class="fancy-select">
    <span data-editor="" class="dropdown block white">
        <span class="option">USA</span>
        <select class="phone-type">
            <option value="Home">Home</option>
            <option value="Mobile">Mobile</option>
            <option value="Work">Work</option>
            <option value="Other">Other</option>
        </select>
    </span>
</div>