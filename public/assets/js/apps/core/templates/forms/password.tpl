<div class="form-error error"></div>

<fieldset class="narrow-fieldset">
    <fieldset class="fieldset-row">
        <div data-fields="currentPassword"></div>
    </fieldset>

    <fieldset class="fieldset-row">
        <div data-fields="newPassword"></div>
    </fieldset>

    <fieldset class="fieldset-row">
        <div data-fields="passwordConfirm"></div>
    </fieldset>

    <fieldset class="fieldset-row">
        <div><label>6-20 characters include at least one letter and number.</label></div>
    </fieldset>
</fieldset>