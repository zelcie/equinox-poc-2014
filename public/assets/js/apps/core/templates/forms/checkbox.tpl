<div class="checkbox">
    <label>
        <span class="icon-check checkbox-replacement"></span>
    </label>
    <span class="label"><%= title %></span>
</div>