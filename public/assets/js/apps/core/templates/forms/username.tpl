<div class="form-error error"></div>

<fieldset class="narrow-fieldset">
    <fieldset class="fieldset-row">
        <div data-fields="email"></div>
    </fieldset>

    <fieldset class="fieldset-row">
        <div data-fields="emailConfirm"></div>
    </fieldset>

    <fieldset class="fieldset-row">
        <div><label>We will send you an email to confirm this change.</label></div>
    </fieldset>
</fieldset>