/* global, define, Backbone */

define(function (require, exports, module) {
    'use strict';

    var Forms    = require('backbone.forms');
    var Template = require('text!core/templates/forms/switch.tpl');

    /**
     * Fancy Checkbox Editor
     *
     * turns our custom checkboxes into Backbone-form friendly checkboxes
     *
     * @public
     */
    Backbone.Form.editors.Switch = Backbone.Form.editors.Base.extend({


        defaultValue: false,

        tagName: 'input',

        events: {
            'click':  function(event) {
                this.trigger('change', this);
            },
            'focus':  function(event) {
                this.trigger('focus', this);
            },
            'blur':   function(event) {
                this.trigger('blur', this);
            },
            'click .switch': 'onClick'
        },

        /**
         * if user clicks on the fake checkbox, update the real one
         */
        onClick: function (e) {
            //e.preventDefault();
            console.warn('CLICKED');

            if(this.$el.prop('checked')) {
                console.log('A');
                this.$el.removeClass('checked');
                this.setValue(false);
            } else {
                console.log('B');
                this.$el.addClass('checked');
                this.setValue(true);
            }

            this.trigger('click', this);
        },

        initialize: function(options) {
            this.options = options || {};

            Backbone.Form.editors.Base.prototype.initialize.call(this, this.options);

            this.$el.attr('type', 'checkbox');

            this.template = this.options.schema.template || this.constructor.template;
        },

        /**
         * Adds the editor to the DOM
         */
        render: function() {
            var options = this.options;
            var schema = this.schema;
            //var input = '<input id="' + this.id + '" name="' + this.key + '" type="hidden" value="' + this.defaultValue + '">';
            var that = this;
            var $el = $($.trim(this.template(schema)));

            $el.append(this.$el);

            $el.addClass(this.id);

            this.value = schema.defaultValue || this.defaultValue;

            this.setValue(this.value);

            this.setElement($el);


            // the events on this editor are not working at all. Same for the original
            // editors. Seems like a bug so using this for now.
            /*$('body').on('click', '.' + this.id, function (e) {
                console.log('onbody clicik');
                //that.onClick(e);
            });*/

            return this;
        },

        getValue: function() {
            return this.$el.prop('checked');
        },

        setValue: function(value) {

            console.warn('#SETVALUE', $('#'+this.id), this.id);
            if (value) {
                this.$el.prop('checked', true);
                $('#' + this.id).val(true);
            }else{
                this.$el.prop('checked', false);
                $('#' + this.id).val(false);
            }
        },

        focus: function() {
            if (this.hasFocus) return;

            this.$el.focus();
        },

        blur: function() {
            if (!this.hasFocus) return;

            this.$el.blur();
        }

    }, {
        template: _.template(Template)
    });

});