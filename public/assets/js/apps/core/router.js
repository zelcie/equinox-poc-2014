/* global define, Marionette */

define(['marionette', 'core/controllers/app'], function (Marionette, Controller) {

    /**
     * Router class
     *
     * This is the Marionette AppRouter class. In here, you should not see any heavy logic; it
     * all has been delegated to the appController. The router is simply directing all routing
     * to the corresponding method in the appController. For instance, if a user hits the url
     * '/billing/overview' it will fire the appController method `billingOverview`.
     *
     * @augments Backbone.Router
     * @name Router
     * @class Router
     * @return routes
     */
    var Router = Marionette.AppRouter.extend({

        controller: new Controller(),

        appRoutes: {

        }

    });

    return Router;
});


/*define(function(require, exports, module) {
    "use strict";

    // External dependencies.
    var Backbone = require("backbone");
    var core = require("./core");

    // Defining the application router.
    module.exports = Backbone.Router.extend({
        routes: {
            "": "index",
            "home": "index",
            "clubs": "clubs",
            "classes": "classes",
            "training": "training",
            "pilates": "pilates",
            "login": "login",
            "error": "error",
            "*path": "goToBackEndRoute"
        },

        index: function() {
//            require([
//                "../home/home"
//            ], function(home){
//                home.init();
//            });
            window.location = "/";
        },

        purchase: function() {
            window.location = "/purchase";
        },

        clubs: function() {
//            require([
//                "../clubs/clubs"
//            ], function(clubs){
//                clubs.init();
//            });
            window.location = "/clubs";
        },

        classes: function() {
            window.location = "/group-fitness";
        },

        pilates: function() {
            window.location = "/pilates";
        },

        login: function() {
//            require([
//                "../login/login"
//            ], function(login){
//                login.init();
//            });
            window.location = "/login";
        },

        goToBackEndRoute: function(){
            // if you get here do a full refresh
            if(window.isSPA) {
                window.location = "/" + path;
            }
        },

        error: function(){
            // Stop route loop here
            // do nothing .NET will take care from here
        }
    });
}); */