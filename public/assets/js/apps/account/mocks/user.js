var user = {
    "EmailAddress": "myeqtestuser14@equinox.com",
    "FacebookId": "100002691080894",
    "BarcodeId": "0825842",
    "FirstName": "Antoinette",
    "LastName": "Harrington",
    "UserId": 290753,
    "Timezone": {
        "Id": "Eastern Standard Time",
        "DisplayName": "(UTC-05:00) Eastern Time (US & Canada)",
        "StandardName": "Eastern Standard Time",
        "DaylightName": "Eastern Daylight Time",
        "BaseUtcOffset": "-05:00:00",
        "AdjustmentRules": [{
            "DateStart": "0001-01-01T00:00:00",
            "DateEnd": "2006-12-31T00:00:00",
            "DaylightDelta": "01:00:00",
            "DaylightTransitionStart": {
                "TimeOfDay": "0001-01-01T02:00:00",
                "Month": 4,
                "Week": 1,
                "Day": 1,
                "DayOfWeek": 0,
                "IsFixedDateRule": false
            },
            "DaylightTransitionEnd": {
                "TimeOfDay": "0001-01-01T02:00:00",
                "Month": 10,
                "Week": 5,
                "Day": 1,
                "DayOfWeek": 0,
                "IsFixedDateRule": false
            }
        }, {
            "DateStart": "2007-01-01T00:00:00",
            "DateEnd": "9999-12-31T00:00:00",
            "DaylightDelta": "01:00:00",
            "DaylightTransitionStart": {
                "TimeOfDay": "0001-01-01T02:00:00",
                "Month": 3,
                "Week": 2,
                "Day": 1,
                "DayOfWeek": 0,
                "IsFixedDateRule": false
            },
            "DaylightTransitionEnd": {
                "TimeOfDay": "0001-01-01T02:00:00",
                "Month": 11,
                "Week": 1,
                "Day": 1,
                "DayOfWeek": 0,
                "IsFixedDateRule": false
            }
        }],
        "SupportsDaylightSavingTime": true
    },
    "SourceSystem": 1
};