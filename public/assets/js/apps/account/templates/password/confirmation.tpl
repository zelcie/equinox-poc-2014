<div class="module tpl-favoriteselector mod-0 dtm-favoriteselector">
    <header class="header-container">
        <h1>Change Password</h1>
    </header>
</div>

<br /><br />

<div class="border-wrapper">
    <div class="membership-info">
        <div class="middle-wrapper">

            <p class="description"><strong>Password changed successfully.</strong></p>

            <p class="description">Please use a new password when you log in next time.</p>
        </div>
    </div>
</div>

<br /><br />

<center>
    <p><a class="black box button large cancel" href="#">Back to Account</a></p>
</center>

