<div class="module tpl-favoriteselector mod-0 dtm-favoriteselector">
    <header class="header-container">
        <h1>Confirmation Sent</h1>
    </header>
</div>

<br /><br />

<div class="border-wrapper">
    <div class="membership-info">
        <div class="middle-wrapper">

            <p class="description"><strong><%= email %></strong></p>

            <p class="description">Sent successfully!<br />
                Click link from your email to confirm this change</p>
        </div>
    </div>
</div>

<br /><br />

<center>
    <p><a class="black box button large cancel" href="#">Back to Account</a></p>
</center>

