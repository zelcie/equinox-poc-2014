<div class="info-block narrow">
    <div>

        <h6><%= name %></h6>

        <p>

            <%= address1 %><br />
            <%= city %>, <%= state %> <%= zipCode %><br />

            <% if( typeof mobilePhone != 'undefined' ) { %>
                <%= mobilePhone %><br />
            <% } else if(typeof homePhone != 'undefined') { %>
                <%= homePhone %><br />
            <% } else if(typeof workPhone != 'undefined') { %>
                <%= workPhone %><br />
            <% } %>

            born on <%= birthDate %><br />
            <a href="#edit">Edit</a>

        </p>

    </div>
</div>

<div class="info-block narrow">
    <div>

        <p>
            <%= email %><br />
            <a href="#username/edit">Change username/email</a><br />
            <a href="#password/edit">Change password</a>
        </p>

    </div>
</div>

<div class="info-block">
    <div>
        <h6>Have question on your current balance?</h6>
        <p>Please contact your club manager to inquire or send us an email at <a href="mailto:info@equinox.com" class="todo">info@equinox.com</a></p>
    </div>
</div>