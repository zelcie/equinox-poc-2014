<% if( accountType === 'residential' ) { %>

    <div class="info-block narrow">
        <div>

            <h4>Your balance</h4>

            <p>Your account is up to date.</p>

        </div>
    </div>

<% } else { %>

    <div class="presentation-table membership-total module">
        <div class="row">

            <div class="col-1-2">
                <div class="col-padding">
                    <small>Your Balance</small>
                    <h3 class="club-name">$<%= displayBalance() %></h3>
                </div>
            </div>

            <div class="col-1-2">
                <div class="col-padding">
                    <% if( !cardLastFourDigits ) { %>
                    <small>You don’t have a credit  card on file</small>

                    <% if( displayBalance() === 0 ) { %>
                    <section class="paragraph rich-content " data-hash="">
                        <nav class="button-container centered">
                            <a href="/account#billing/add" class="white box button small">Add Card</a>
                        </nav>
                    </section>
                    <% } %>
                    <% } %>

                    <% if( cardLastFourDigits ) { %>
                    <small>Your Card</small>

                    <h3 class="club-name">
                        <%= cardEnding() %>
                        <% if( isCardExpired ) { %>
                        <div style="color:red">Expired</div>
                        <% } %>
                        <small class="todo">Auto Purchase: <%= autoPurchase() %></small>
                    </h3>

                    <% if( displayBalance() === 0 ) { %>
                    <a href="#billing/update" class="cta">Change Card</a>
                    <% } %>
                    <% } %>
                </div>
            </div>

        </div>
    </div>

    <% if( isCardExpired && currentBalance > 0 && cardLastFourDigits != null ) { %>
    <section class="paragraph rich-content " data-hash="">
        <nav class="button-container centered">
            <a href="/account#billing/payment" class="white box button large">PAY WITH NEW CARD</a>
        </nav>
    </section>
    <% } %>

    <% if( !cardLastFourDigits && currentBalance > 0 ) { %>
    <section class="paragraph rich-content " data-hash="">
        <nav class="button-container centered">
            <a href="/account#billing/payment" class="white box button large">Pay Due Now</a>
        </nav>
    </section>
    <% } %>

<% } %>