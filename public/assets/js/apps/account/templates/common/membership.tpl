<div class="info-block narrow">
    <div>

        <h4>Your select membership</h4>

        <p>
            <%= facility %><br />
            <%= membershipDuration() %><br />
            <%= contractInfo() %><br />

            <% if(membershipStatus == 'Expired') { %>
                <a href="/classic/MYEQ/Renewal/ChooseAccess.aspx">Renew</a>
            <% } else {%>
                <a href="/Settings/freeze/contact-info">Freeze</a>
            <% } %>
        </p>

    </div>
</div>