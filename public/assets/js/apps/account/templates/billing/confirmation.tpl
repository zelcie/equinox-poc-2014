<div class="module tpl-favoriteselector mod-0 dtm-favoriteselector">
    <header class="header-container">
        <h1><%= thanks() %></h1>
    </header>
</div>

<div class="border-wrapper">
    <div class="membership-info">
        <div class="middle-wrapper">

            <h3 class="club-name"><small><%= msg() %></small></h3>

            <% if( confirmation() !== '' ) { %>
                <p class="description">Confirmation # <%= confirmation() %></p>
            <% } %>
        </div>
    </div>
</div>

<div class="membership-total module">
    <div class="border-wrapper">

        <div class="col location">
            <% if( confirmation() !== '' ) { %>
                <h4>Amount Charged</h4>
                <p>$<%= currentBalance %></p>
            <% } %>
        </div>

        <div class="col due-total">
            <div><%= cardEnding() %></div>
            <div><%= autoPurchase() %></div>
            <div><%= nameOnCard %></div>
        </div>

    </div>
</div>

<div>The receipt was sent to email: <%= email %></div>

<div><a class="link" href="/account#billing/update">Edit</a></div>
<p><a class="link" href="#">Print this page</a></p>
<p><a class="link" href="/account#billing/info">Back to Account</a></p>

