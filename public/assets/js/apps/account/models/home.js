define(function (require, exports, module) {
    'use strict';

    // External dependencies.
    var Backbone        = require('backbone');

    /**
     * Account HP Model
     *
     * Model for the account homepage
     *
     * @augments Backbone.Model
     * @name AccountHome
     * @class AccountHomeModel
     * @return model
     */
    var Model = Backbone.Model.extend({

        //url: consts.API + '/billing/billing-information',

        defaults: {
            title: 'Account Info'
        }

    });

    module.exports = Model;
});
