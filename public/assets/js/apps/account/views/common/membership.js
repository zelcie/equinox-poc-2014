/* global define, moment, Marionette, _ */

define(function (require, exports, module) {
    'use strict';

    require('momentjs');

    var Template   = require('text!templates/common/membership.tpl');

    /**
     * Partial View Sample
     *
     * Regular Marionette ItemView Partial
     *
     * @name Partial1
     * @class PartialView1
     * @return view
     */
    var View = Marionette.ItemView.extend({

        template: _.template(Template),

        templateHelpers: {
            membershipDuration: function () {

                var startDate = moment(this.startDate).format('MMM D. \'YY');
                var expirationDate = moment(this.expirationDate).format('MMM D. \'YY');

                return startDate + ' - ' + expirationDate;
            },

            contractInfo: function () {
                var str = '';

                if (this.obligationDate !== null) {
                    str = 'Current rate applies until ' + moment(this.obligationDate).format('MMM D. \'YY');
                } else {
                    var expirationDate = moment(this.expirationDate);

                    if ( this.membershipStatus !== 'Expired' ) {
                        str = moment(expirationDate, "YYYYMMDD").fromNow() + ' contract until ' + moment(this.expirationDate).format('MMM D. \'YY');
                    } else {
                        str = '<span class="error">Your Membership has expired!</span>'
                    }
                }

                return str;
            }
        }

    });

    module.exports = View;
});