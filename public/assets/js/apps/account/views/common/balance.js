define(function (require, exports, module) {
    'use strict';

    var Core       = require('core/app');
    var Marionette = require('marionette');
    var Template   = require('text!templates/common/balance.tpl');

    /**
     * Partial View Sample
     *
     * Regular Marionette ItemView Partial
     *
     * @name Partial1
     * @class PartialView1
     * @return view
     */
    var View = Marionette.ItemView.extend({

        template: _.template(Template),

        templateHelpers: {

            /**
             * displayBalance
             *
             * The balance displayed to the user can be different from the currentBalance
             * for various reasons like, if autopayment is on and the card is still valid, etc.
             *
             * @returns {number}
             */
            displayBalance: function() {
                var balance = (this.currentBalance <= 0) ? 0 : this.currentBalance;

                // User has a balance, has valid card on file but autopay is on
                if (this.currentBalance > 0 && !this.isBillingOptOut && (this.isCardExpired && !this.cardLastFourDigits)) {
                    balance = 0;
                }

                // if you have a balance but card has not expired
                if (this.currentBalance > 0 && !this.isCardExpired) {
                    balance = 0;
                }

                return balance;
            },

            autoPurchase: function() {
                var autopay = this.isBillingOptOut ? 'OFF' : 'ON';
                return autopay;
            },

            cardEnding: function() {
                var cardNumber = this.creditCardNumber ? this.creditCardNumber : null;
                var lastFour   = this.cardLastFourDigits ? this.cardLastFourDigits : Core.utils.getLastFourDigits(cardNumber);
                var cardType   = this.cardType ? this.cardType : Core.utils.getCardType(creditCardNumber);

                return cardType + ' ending - ' + lastFour;
            }

        }

    });

    module.exports = View;
});