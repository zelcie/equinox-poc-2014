define(function (require, exports, module) {
    'use strict';

    var Core            = require('core/app');
    var Marionette      = require('marionette');
    var BossView        = require('bossview');
    var HeadingView     = require('core/views/common/heading');
    var AccountFormView = require('core/views/forms/account');
    var SubmitView      = require('core/views/forms/submit');
    var NameView        = require('core/views/forms/static_fieldsets');

    /**
     * Account Homepage View
     *
     * This is the master view for the account homepage
     *
     * @name Page
     * @class PageView
     * @return view
     */
    var View = Marionette.BossView.extend({

        template: function() {
            return '<div class="heading-subregion"></div><form class="large black forms-spa"></form>';
        },

        subViews: {
            heading: HeadingView,
            name: function() {

                this.model.set({
                    fieldsets: [
                        { label: 'Member Name', content: this.model.get('name') }
                    ]
                });

                return new NameView({
                    model: this.model
                });
            },
            form: AccountFormView,
            submit: SubmitView
        },

        /**
         * subViewContainers
         *
         * Dump the subviews in the following areas of this view's template
         */
        subViewContainers: {
            heading:   '.heading-subregion',
            name:      'form',
            form:      'form',
            submit:    'form'
        },

        subViewEvents: {
            'submit submit': 'onSubmit'
        },

        /**
         * onSubmit
         *
         * There is a click event on the .submit button which we listen to.
         * If clicked, we first run the form.commit() which does the form
         * validation. If no object is returned in the errors object, we
         * save to the model and submit the form via ajax. If successfull,
         * go to the confirmation page. If it fails, show error to user.
         *
         * @name BillingPaymentView#onSubmit
         * @function
         * @public
         */
        onSubmit: function(e) {
            var errs, that = this;

            errs = this.form.commit();

            Core.store.set('account', this.model.attributes);

            if (_.isEmpty(errs)) {
                this.form.model.save()
                    .success(function(model, response) {
                        Core.account.Router.navigate('', {trigger: true});
                    })
                    .error(function(data) {
                        that.$el.find('.form-error').text(data.responseJSON.message);
                        console.log('error:', data);
                    });
            } else {
                console.warn('Form validation error. Aborting form submission...', errs)
            }

        },

        submitForm: function(that, opts) {
            var errs = that.form.commit();

            Core.store.set(opts.store, that.model.attributes);

            if (_.isEmpty(errs)) {
                that.form.model.save()
                    .success(function(model, response) {
                        if(opts.next) {
                            Core.account.Router.navigate(opts.next, {trigger: true});
                        }
                        opts.success();
                    })
                    .error(function(data) {
                        that.$el.find('.form-error').text(data.responseJSON.message);
                        opts.error();
                        console.log('error:', data);
                    });
            } else {
                console.warn('Form validation error. Aborting form submission...', errs)
            }
        },

        onSubmitv2: function() {

            function submitForm(next, opts) {};

            submitForm(this, {
                next: 'confirmation',
                store: Core.store.billing, // optional
                success: function() {}, // optional
                error: function() {} // optional
            });
        }

    });

    module.exports = View;
});