/* global define, Backbone, Marionette, App, consts */

define(function (require, exports, module) {
    'use strict';

    var Marionette      = require('marionette');
    var BillingModel    = require('models/billing');
    var MembershipModel = require('models/membership');
    var Model           = require('models/home');
    var View            = require('views/home');
    var Core            = require('core/app');

    /**
     * Account Info/Home Controller
     *
     * Controller used to manage everything for the /account homepage
     *
     * @augments Backbone.Model
     * @name AccountHome
     * @class AccountHomeController
     * @return model
     */
    var Controller = Marionette.Controller.extend({

        /**
         * Init
         *
         * @name AccountHomeController#init
         * @function
         * @public
         */
        init: function(id) {
            var model, view;

            var billingModel = new BillingModel();
            var membershipModel = new MembershipModel();

            if (id) {
                billingModel.url = consts.MOCKS + '/billing/' + id + '.json';
            }

            billingModel.fetch({async:false});
            membershipModel.fetch({async:false});

            model = new Model({ billing: billingModel, membership: membershipModel });
            view  = new View({ model: model });

            Core.addRegions({ mainRegion: Core.el });
            Core.mainRegion.show(view);

        }

    });

    module.exports = Controller;
});