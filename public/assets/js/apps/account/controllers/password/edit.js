/* global define, App, _ */

define(function (require, exports, module) {
    'use strict';

    var Controller;
    var Marionette = require('marionette');
    var Model      = require('models/password');
    var View       = require('views/password/edit');
    var Core       = require('core/app');

    /**
     * Edit Password Controller
     *
     * Controller used to edit /username/edit
     *
     * @augments Backbone.Model
     * @name Username
     * @class UsernameController
     * @return model
     */
    Controller = Marionette.Controller.extend({

        addToRegion: function (view) {
            Core.addRegions({ mainRegion: Core.el });
            Core.mainRegion.show(view);
        },

        /**
         * Init
         *
         * @name ProfileEditController#init
         * @function
         * @public
         */
        init: function () {
            var model, view;

            model = new Model();
            view  = new View({ model: model });

            this.addToRegion(view);
        }

    });

    module.exports = Controller;
});