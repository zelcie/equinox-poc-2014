define(function (require, exports, module) {
    'use strict';

    var $ = require('jquery');

    // Alias the module for easier identification.
    var login = module.exports;

    login.init = function () {
        var html = '<h1>Login Module </h1>';
        $('.page').html(html);
    };

    return login;
});