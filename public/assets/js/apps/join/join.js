define(function(require, exports, module) {

    var $ = require('jquery');

    // Alias the module for easier identification.
    var home = module.exports;

    home.say = function(word){
        return word;
    };

    home.init = function(){
        var html = '<h1>Home Module </h1>';
        $('.page').html(html);
    };

    return home;
});