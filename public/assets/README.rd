# Installation Instructions

## Ruby Gems
sudo gem install susy
sudo gem install compass
sudo gem install compass-rgbapng

# If you get issues trying to run grunt, saying that it could not load 
# compass then, uninstall all sass gems, compass gem and now install
# and old version like so:
sudo gem install compass --pre

# That will install a version of compass that plays nice with Susy
# If the problem persists on a Mac, try upgrading to the latest OS
# (as of now, that would be Mavericks
