exports.tiers= function (req, res) {
    'use strict';

    var json = require('../public/v1/personal-training-purchase/tiers');
    console.warn('GET. Server response: ' + res.statusCode);
    console.dir(json);

    res.send(JSON.stringify(json));
};

exports.packageSize = function (req, res) {
    'use strict';

    var json = require('../public/v1/personal-training-purchase/packsize');
    console.warn('GET. Server response: ' + res.statusCode);
    console.dir(json);

    res.send(JSON.stringify(json));
};

exports.inventory = function (req, res) {
    'use strict';

    var json = require('../public/v1/personal-training-purchase/inventory');
    console.warn('GET. Server response: ' + res.statusCode);
    console.dir(json);

    res.send(JSON.stringify(json));
};

exports.billingInfo = function (req, res) {
    'use strict';

    var json = require('../public/v1/personal-training-purchase/billing-information');
    console.warn('GET. Server response: ' + res.statusCode);
    console.dir(json);

    res.send(JSON.stringify(json));
};


exports.validatePassword = function (req, res) {
    'use strict';

    var json = require('../public/v1/personal-training-purchase/validate-password');
    console.warn('POST. Server response: ' + res.statusCode);
    console.dir(req.body);

    res.send(JSON.stringify(json));
};

exports.purchaseWithCardOnFile = function (req, res) {
    'use strict';

    console.info('POST purchase with card on file. Server response: ' + res.statusCode);
    console.dir(req.body);

    res.send(JSON.stringify({ orderNumber: '100158102639' }));
};


exports.purchasePackage = function (req, res) {
    'use strict';

    console.info('POST purchase. Server response: ' + res.statusCode);
    console.dir(req.body);

    res.send(JSON.stringify({ orderNumber: '100158102639' }));
};


// Personal Training Schedule API
exports.scheduleCancel = function (req, res) {
    'use strict';
    console.info('PUT schedule. Server response: ' + res.statusCode);
    console.dir(req.body);

    var json = require('../public/v1/personal-training-schedule/cancel');

    res.send(JSON.stringify(json));
};

exports.scheduleUpdate = function (req, res) {
    'use strict';
    console.info('POST schedule. Server response: ' + res.statusCode);
    console.dir(req.body);
    var json = require('../public/v1/personal-training-schedule/reschedule');

    res.send(JSON.stringify(json));
};

exports.scheduleCreate = function (req, res) {
    'use strict';
    console.info('POST schedule. Server response: ' + res.statusCode);
    console.dir(req.body);
    var json = require('../public/v1/personal-training-schedule/create');
    res.send(JSON.stringify(json));
};

exports.scheduleAppointments = function (req, res) {
    'use strict';
    var json = require('../public/v1/personal-training-schedule/appointments');
    console.warn('GET. Server response: ' + res.statusCode);
    console.dir(json);
    res.send(JSON.stringify(json));
};

exports.scheduleAppointment = function (req, res) {
    'use strict';
    var json = require('../public/v1/personal-training-schedule/appointment');
    console.warn('GET. Server response: ' + res.statusCode);
    console.dir(json);
    res.send(JSON.stringify(json));
};

exports.scheduleError = function (req, res) {
    'use strict';
    var json = require('../public/v1/personal-training-schedule/appointment-error');
    console.warn('GET. Server response: ' + res.statusCode);
    console.dir(json);
    res.send(JSON.stringify(json));
};

exports.account = function (req, res) {
    'use strict';
    console.info('POST schedule. Server response: ' + res.statusCode);
    console.dir(req.body);
    res.send(JSON.stringify({success:true}));
};
