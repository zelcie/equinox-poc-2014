exports.index = function(req, res){
    res.render('index', { title: 'Equinox Home Page' });
};

exports.clubs = function(req, res){
    res.render('clubs', { title: 'Clubs' });
};

exports.groupFitness = function(req, res){
    res.render('group-fitness', { title: 'Classes' });
};

exports.personalTraining = function(req, res){
    res.render('personal-training', { title: 'Personal Training' });
};

exports.personalTrainingPurchase = function(req, res){
    res.render('purchase', { title: 'Personal Training Purchase' });
};

exports.personalTrainingPurchaseSchedule = function(req, res){
    res.render('schedule', { title: 'Personal Training Schedule' });
};

exports.personalTrainingRules = function(req, res){
    res.render('rules', { title: 'Personal Training Rules' });
};

exports.account = function(req, res){
    res.render('account', { title: 'Account Billing' });
};

exports.othersettings = function(req, res){
    res.render('othersettings', { title: 'Other Settings' });
};

exports.pilates = function(req, res){
    res.render('pilates', { title: 'Pilates' });
};

exports.spa = function(req, res){
    res.render('spa', { title: 'Spa' });
};

exports.join = function(req, res){
    res.render('join', { title: 'Join' });
};

exports.login = function(req, res){
    res.render('login', { title: 'Login' });
};

exports.visitUs= function(req, res){
    res.render('visit-us', { title: 'Visit Us' });
};

exports.scheduleEquifit = function(req, res){
    res.render('schedule-equifit', { title: 'Schedule Equifit' });
};

exports.priceList = function(req, res){
    res.render('pricelist', { title: 'Price List' });
};