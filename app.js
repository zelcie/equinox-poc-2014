
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var api = require('./routes/api');
var http = require('http');
var path = require('path');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
//app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));
//app.use(express.static(path.join(__dirname, 'assets')));

// set pretty html. coment for production
app.locals.pretty = true;

// development only
if ('development' == app.get('env')) {
    app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/clubs', routes.clubs);
app.get('/group-fitness', routes.groupFitness);

app.get('/personal-training', routes.personalTraining);
app.get('/personal-training/schedule-equifit', routes.scheduleEquifit);
app.get('/personal-training/pricelist', routes.priceList);

app.get('/personal-training/purchase*', routes.personalTrainingPurchase);
app.get('/personal-training/schedule*', routes.personalTrainingPurchaseSchedule);
app.get('/personal-training/rules', routes.personalTrainingRules);
app.get('/account', routes.account);
app.get('/othersettings', routes.othersettings);

app.get('/pilates', routes.pilates);
app.get('/spa', routes.spa);
app.get('/join/step1', routes.join);
app.get('/login', routes.login);
app.get('/visit-us', routes.visitUs);



// Purchase API
app.get('/v1/personal-training-purchase/tiers', api.tiers);
app.get('/v1/personal-training-purchase/packsize*', api.packageSize);
app.get('/v1/personal-training-purchase/inventory', api.inventory);
app.get('/v1/personal-training-purchase/billing-information', api.billingInfo);
app.post('/v1/personal-training-purchase/validate-password', api.validatePassword);
app.post('/v1/personal-training-purchase/purchase-package-with-card-on-file', api.purchaseWithCardOnFile);
app.post('/v1/personal-training-purchase/purchase-package', api.purchasePackage);

// Schedule API
app.get('/v1/personal-training-schedule/appointments', api.scheduleAppointments);
app.get('/v1/personal-training-schedule/appointments/123', api.scheduleAppointment);
//app.get('/v1/personal-training-schedule/appointments/1234', api.scheduleError);
app.put('/v1/personal-training-schedule/cancel', api.scheduleCancel);
app.put('/v1/personal-training-schedule/update', api.scheduleUpdate);
app.post('/v1/personal-training-schedule/create', api.scheduleCreate);
//app.get('/v1/personal-training-schedule/appointment-error', api.scheduleError);


// Account API
app.post('/v1/account', api.account);
app.post('/V1/account/account-information/update', api.account);
app.post('/V1/account/password/update', api.account);
app.post('/V1/account/username/update', api.account);
app.post('/assets/js/apps/account/mocks/othersettings/nplogin.json', api.account);
app.post('/assets/js/apps/account/mocks/othersettings/npregister.json', api.account);

http.createServer(app).listen(app.get('port'), function () {
  console.log('Express server listening on port ' + app.get('port'));
});
